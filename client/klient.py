import cx_Oracle 
from tkinter import *
import tkinter as tk
from tkinter import ttk
from tkinter.scrolledtext import ScrolledText
import sys


def zestawienie_roku():
 """
 Funkcje w programie
 do procedury  o  nr  1 w pakiecie (zakladka 6) 
 """
 try: 
   dsn_t = cx_Oracle.makedsn("localhost", "1521", "orcl")
   polaczenie = cx_Oracle.connect(user="projekt",password="123",dsn=dsn_t)
   cur = polaczenie.cursor()
   dobierz = pole_zestawienie_roku.get()
   if dobierz:
       raise Exception("pole_zestawienie_roku jest puste")
   rok = int(dobierz)
   if rok > 2020 or rok < 1980 and rok != 0 :
       raise Exception("podales rok ktory jest powyzej 2020 lub jest mniejszy od 1980")
   cur.callproc("dbms_output.enable")
   cur.callproc('biuro_podrozy.zestawienie_rok',[rok])
   wynik1 = cur.var(cx_Oracle.STRING)
   wynik2 = cur.var(cx_Oracle.NUMBER)
   text_zak6.configure(state='normal')
   #wyczyszenie pola
   text_zak6.delete('1.0', END)
   while TRUE:
     cur.callproc("dbms_output.get_line",(wynik1,wynik2))
     if wynik2.getvalue() != 0:
           break
     text_zak6.insert(END,wynik1.getvalue())
     text_zak6.insert(END,'\n')
   cur.close()
   polaczenie.close()
   text_zak6.configure(state='disabled')
 except Exception as puste_pole:
   text_zak6.configure(state='normal')
   text_zak6.delete('1.0', END)
   text_zak6.insert(END,puste_pole)
   text_zak6.configure(state='disabled')
 except Exception as  zly_rok:
   text_zak6.configure(state='normal')
   text_zak6.delete('1.0', END)
   text_zak6.insert(END,zly_rok)
   text_zak6.configure(state='disabled')
 except ValueError:
   text_zak6.configure(state='normal')
   text_zak6.delete('1.0', END)
   text_zak6.insert(END,"rok nie moze byc wyrazem ")
   text_zak6.configure(state='disabled')

def dodawanie_danych_klienta():
  """
  do funkcji  o  nr  2 w pakiecie (zakladka 3) 
  """
  try:
    dsn_t = cx_Oracle.makedsn("localhost", "1521", "orcl")
    polaczenie = cx_Oracle.connect(user="projekt",password="123",dsn=dsn_t)
    cur = polaczenie.cursor()
    nowy_imie_klienta  = pole_klient_imie_zak3.get()
    nowy_nazwisko_klienta =  pole_klient_nazwisko_zak3.get()
    if nowy_imie_klienta  or  nowy_nazwisko_klienta:
       raise Exception("nie podales imienia lub nazwiska klienta ")
    ilosc_osob = str(pole_ilosc_osob_zak3.get())
    rodzaj_zamowienia = pole_rodzaj_zamowienie_zak3.get()
    if rodzaj_zamowienia == "" or ilosc_osob == "":
       raise Exception("nie podales ulgi lub ilosc osob")
    miasto = pole_miasto_zak3.get()
    srodek_transportu =  pole_srodek_transportu_zak3.get()
    if miasto or srodek_transportu:
       raise Exception("nie wprowadziles miasta lub transportu")
    if rodzaj_zamowienia != 'rodzinny' and rodzaj_zamowienia != 'normalny' and rodzaj_zamowienia != 'seniorski' and rodzaj_zamowienia != 'dla par' and rodzaj_zamowienia !='firmowy'  :
       raise Exception("nie ma takiej  dostepnej ulgi sa dostepne: normalny ,rodzinny,seniorski , dla par , firmowy")
    if srodek_transportu != 'autobus' and srodek_transportu != 'samolot' and srodek_transportu != 'pociag':
       raise Exception("nie ma takiej  dostepnego srodka transportu dostepne sa: autobus , pociag , samolot")
    ile_osob = int(ilosc_osob)
    if ile_osob > 999:
        raise Exception("nie mozna dodaj tulu osob")
    zwracana_wartosc = cur.var(cx_Oracle.STRING)
    cur.callfunc('biuro_podrozy.wprowadzanie',zwracana_wartosc,[nowy_imie_klienta 
      ,nowy_nazwisko_klienta,ile_osob,rodzaj_zamowienia,miasto,srodek_transportu])
    text_zak3.configure(state='normal')
    #wyczyszenie pola
    text_zak3.delete('1.0', END)
    text_zak3.insert(END,zwracana_wartosc.getvalue())
    text_zak3.configure(state='disabled')
    cur.close()
    polaczenie.close()
  except Exception as pusta_wartosc:
    text_zak3.configure(state='normal')
    text_zak3.delete('1.0', END)
    text_zak3.insert(END,pusta_wartosc)
    text_zak3.configure(state='disabled')
  except Exception as niewlasciwa_wartosc:
    text_zak3.configure(state='normal')
    text_zak3.delete('1.0', END)
    text_zak3.insert(END,niewlasciwa_wartosc)
    text_zak3.configure(state='disabled')
  except Exception as przekroczenie:
    text_zak3.configure(state='normal')
    text_zak3.delete('1.0', END)
    text_zak3.insert(END,przekroczenie)
    text_zak3.configure(state='disabled')
  except ValueError:
    text_zak3.configure(state='normal')
    text_zak3.delete('1.0', END)
    text_zak3.insert(END,"wprowadziles stringa w  polu ilosc_osob")
    text_zak3.configure(state='disabled')
  except cx_Oracle.Error as error:
    text_zak3.configure(state='normal')
    text_zak3.delete('1.0', END)
    text_zak3.insert(END,error)
    text_zak3.configure(state='disabled')    

def usuwanie_danych_klienta():
  """
  do funkcji  o  nr  3 w pakiecie (zakladka 4) 
  """
  try: 
    dsn_t = cx_Oracle.makedsn("localhost", "1521", "orcl")
    polaczenie = cx_Oracle.connect(user="projekt",password="123",dsn=dsn_t)
    cur = polaczenie.cursor()
    imie = pole_klient_imie_zak4.get()
    nazwisko = pole_klient_nazwisko_zak4.get() 
    zamowienie = pole_id_zamowienia_zak4.get() 
    if imie == "" or nazwisko == "" or zamowienie == "":
        raise Exception("nie podales wartosc imienia lub nazwiska lub id_zamowienia")
    id_zamowienie = int(zamowienie)
    if id_zamowienie > 999999999999999:
       raise Exception("nie mozna usunac takiego id_zamowienia przekroczono typ")
    zwracana_wartosc = cur.var(cx_Oracle.STRING)
    cur.callfunc('biuro_podrozy.usuwanie',zwracana_wartosc,[imie,nazwisko, id_zamowienie])
    text_zak4.configure(state='normal')
    #wyczyszenie pola
    text_zak4.delete('1.0', END)
    text_zak4.insert(END,zwracana_wartosc.getvalue())
    cur.close()
    polaczenie.close()
    text_zak4.configure(state='disabled')
  except Exception as pusta_wartosc:
    text_zak4.configure(state='normal')
    text_zak4.delete('1.0', END)
    text_zak4.insert(END,pusta_wartosc)
    text_zak4.configure(state='disabled')
  except Exception as przekroczenie:
    text_zak4.configure(state='normal')
    text_zak4.delete('1.0', END)
    text_zak4.insert(END,przekroczenie)
    text_zak4.configure(state='disabled')
  except ValueError:
    text_zak4.configure(state='normal')
    text_zak4.delete('1.0', END)
    text_zak4.insert(END,"wprowadziles stringa w  polu id_zamowienia")
    text_zak4.configure(state='disabled')
  except cx_Oracle.Error as error:
    text_zak4.configure(state='normal')
    text_zak4.delete('1.0', END)
    text_zak4.insert(END,error)
    text_zak4.configure(state='disabled')   
 
def aktualizowanie_danych_klienta():
   """
   do funkcji  o  nr  4 w pakiecie (zakladka 5)
   """
   try:
     dsn_t = cx_Oracle.makedsn("localhost", "1521", "orcl")
     polaczenie = cx_Oracle.connect(user="projekt",password="123",dsn=dsn_t)
     cur = polaczenie.cursor()
     imie = pole_klient_imie_zak5.get() 
     nazwisko = pole_klient_nazwisko_zak5.get()
     zamowienie = pole_wpr_id_zamowienia_zak5.get()  
     if imie ==  "" or nazwisko == "" or zamowienie == "":
         raise Exception("nie podales wartosc imienia lub nazwiska lub id_zamowienia") 
     nowe_imie = pole_nowe_imie_zak5.get()  
     nowe_nazwisko = pole_nowe_nazwisko_zak5.get()
     if nowe_imie == "" or nowe_nazwisko == "":
          raise Exception("nie podales wartosc nowego imienia lub nazwiska ")
     zmiana_ulgi = pole_zmiana_ulgi_zak5.get()   
     zmiana_transportu = pole_nowy_transport_zak5.get()  
     if zmiana_ulgi == "" or zmiana_transportu == "":
         raise Exception("nie podales wartosc nowej ulgi lub transportu ")
     id_zamowienie  = int(zamowienie)
     if id_zamowienie > 999999999999999:
       raise Exception("nie mozna usunac takiego id_zamowienia przekroczono typ")
     zwracana_wartosc = cur.var(cx_Oracle.STRING)
     cur.callfunc('biuro_podrozy.aktualizowanie',zwracana_wartosc,[imie,nazwisko,id_zamowienie,
     nowe_imie,nowe_nazwisko,zmiana_ulgi,zmiana_transportu])
     text_zak5.configure(state='normal')
     #wyczyszenie pola
     text_zak5.delete('1.0', END)
     text_zak5.insert(END,zwracana_wartosc.getvalue())
     cur.close()
     polaczenie.close()
   except Exception as pusta_wartosc:
    text_zak5.configure(state='normal')
    text_zak5.delete('1.0', END)
    text_zak5.insert(END,pusta_wartosc)
    text_zak5.configure(state='disabled')
   except Exception as przekroczenie:
    text_zak5.configure(state='normal')
    text_zak5.delete('1.0', END)
    text_zak5.insert(END,przekroczenie)
    text_zak5.configure(state='disabled')
   except ValueError:
    text_zak5.configure(state='normal')
    text_zak5.delete('1.0', END)
    text_zak5.insert(END,"wprowadziles stringa w  polu id_zamowienia lub zmiana ulgi")
    text_zak5.configure(state='disabled')
   except cx_Oracle.Error as error:
    text_zak5.configure(state='normal')
    text_zak5.delete('1.0', END)
    text_zak5.insert(END,error)
    text_zak5.configure(state='disabled')   

def rodzaj_transportu():
   """
   do procedury  o  nr  5 w pakiecie  (zakladka7) 
   """
   dsn_t = cx_Oracle.makedsn("localhost", "1521", "orcl")
   polaczenie = cx_Oracle.connect(user="projekt",password="123",dsn=dsn_t)
   cur = polaczenie.cursor()
   srodek_transportu = ustaw_wartosc.get()
   cur.callproc("dbms_output.enable")
   cur.callproc('biuro_podrozy.rodzaj_tran',[srodek_transportu])
   wynik1 = cur.var(cx_Oracle.STRING)
   wynik2 = cur.var(cx_Oracle.NUMBER)
   text_zak7.configure(state='normal')
   #wyczyszenie pola
   text_zak7.delete('1.0', END)
   while TRUE:
     cur.callproc("dbms_output.get_line",(wynik1,wynik2))
     if wynik2.getvalue() != 0:
           break
     text_zak7.insert(END,wynik1.getvalue())
     text_zak7.insert(END,'\n')
   cur.close()
   polaczenie.close()
   text_zak7.configure(state='disabled')


def suma_z_ulga():
 """
 do procedury  o  nr  6 w pakiecie (zakladka 8)
 """
 try:
   dsn_t = cx_Oracle.makedsn("localhost", "1521", "orcl")
   polaczenie = cx_Oracle.connect(user="projekt",password="123",dsn=dsn_t)
   cur = polaczenie.cursor()
   ulga = pole_zamowienie_rodzaj_zak8.get()
   if ulga == "":
     raise Exception("Nie podales rodzaju ulgi")
   cur.callproc("dbms_output.enable")
   cur.callproc('biuro_podrozy.suma_z_ulga',[ulga])
   wynik1 = cur.var(cx_Oracle.STRING)
   wynik2 = cur.var(cx_Oracle.NUMBER)
   text_zak8.configure(state='normal')
   #wyczyszenie pola
   text_zak1.delete('1.0', END)
   while TRUE:
     cur.callproc("dbms_output.get_line",(wynik1,wynik2))
     if wynik2.getvalue() != 0:
           break
     text_zak8.insert(END,wynik1.getvalue())
     text_zak8.insert(END,'\n')
   cur.close()
   polaczenie.close()
   text_zak8.configure(state='disabled')
 except Exception as pusty:
   text_zak8.configure(state='normal')
   text_zak8.delete('1.0', END)
   text_zak8.insert(END,pusty)
   text_zak8.configure(state='disabled')
 except ValueError:
   text_zak8.configure(state='normal')
   text_zak8.delete('1.0', END)
   text_zak8.insert(END,"wartosc nie byla wlasciwego typu ")
   text_zak8.configure(state='disabled')


def wyswietl_dane_klienta():
  """
  do procedury  o  nr  7 w pakiecie (zakladka 1)
  """
  try:  
    dsn_t = cx_Oracle.makedsn("localhost", "1521", "orcl")
    polaczenie = cx_Oracle.connect(user="projekt",password="123",dsn=dsn_t)
    cur = polaczenie.cursor()
    imie_kl = pole_klient_imie_zak1.get()
    nazwisko_kl = pole_klient_nazwisko_zak1.get()
    if imie_kl == "":
           raise Exception("Nie podales  imienia klienta ")
    if nazwisko_kl == "":
            raise Exception("Nie podales  nazwiska klienta ")
    cur.callproc("dbms_output.enable")
    cur.callproc('biuro_podrozy.dane_klient_wys',[imie_kl,nazwisko_kl])
    wynik1 = cur.var(cx_Oracle.STRING)
    wynik2 = cur.var(cx_Oracle.NUMBER)
    text_zak1.configure(state='normal')
    #wyczyszenie pola
    text_zak1.delete('1.0', END)
    while TRUE:
       cur.callproc("dbms_output.get_line",(wynik1,wynik2))
       if wynik2.getvalue() != 0:
            break
       text_zak1.insert(END,wynik1.getvalue())
       text_zak1.insert(END,'\n')
    cur.close()
    polaczenie.close()
    text_zak1.configure(state='disabled')
  except Exception as pusty:
       text_zak1.configure(state='normal')
       text_zak1.delete('1.0', END)
       text_zak1.insert(END,pusty)
       text_zak1.configure(state='disabled')
  except ValueError:
       text_zak1.configure(state='normal')
       text_zak1.delete('1.0', END)
       text_zak1.insert(END,"wartosc nie byla wlasciwego typu ")
       text_zak1.configure(state='disabled')


def wyswietl_kraj_miasto():
    """
    do procedury  o  nr  8 w pakiecie (zakladka 2)
    """
    try:
        dsn_t = cx_Oracle.makedsn("localhost", "1521", "orcl")
        polaczenie = cx_Oracle.connect(user="projekt",password="123",dsn=dsn_t)
        cur = polaczenie.cursor()
        kraj = pole_kraj_zak2.get()
        if kraj == "":
           raise Exception("Nie podales zadnej wartosc do pola kraj ")
        miasto = pole_miasto_zak2.get()
        cur.callproc("dbms_output.enable")
        cur.callproc('biuro_podrozy.wyswietl_klientowi',[kraj,miasto])
        wynik1 = cur.var(cx_Oracle.STRING)
        wynik2 = cur.var(cx_Oracle.NUMBER)
        text_wycieczki.configure(state='normal')
        #wyczyszenie pola
        text_wycieczki.delete('1.0', END)
        while TRUE:
            cur.callproc("dbms_output.get_line",(wynik1,wynik2))
            if wynik2.getvalue() != 0:
                break
            text_wycieczki.insert(END,wynik1.getvalue())
            text_wycieczki.insert(END,'\n')
        cur.close()
        polaczenie.close()
        text_wycieczki.configure(state='disabled')
    except Exception as pusty:
        text_wycieczki.configure(state='normal')
        text_wycieczki.delete('1.0', END)
        text_wycieczki.insert(END,pusty)
        text_wycieczki.configure(state='disabled')
    except ValueError:
        text_wycieczki.configure(state='normal')
        text_wycieczki.delete('1.0', END)
        text_wycieczki.insert(END,"wartosc nie byla wlasciwego typu ")
        text_wycieczki.configure(state='disabled') 

#zakladka 9 funkcje
def wyswietl_tabel_kl():
    dsn_t = cx_Oracle.makedsn("localhost", "1521", "orcl")
    polaczenie = cx_Oracle.connect(user="projekt",password="123",dsn=dsn_t)
    test1 = polaczenie.cursor()
    zapytanie = 'select * from projekt.klient'
    test1.execute(zapytanie)
    text_zak9.configure(state='normal')
    text_zak9.delete('1.0', END)
    for row in test1:
        calosc = row[0], '-', row[1], '-' ,row[2] 
        text_zak9.insert(END, calosc)
        text_zak9.insert(END,'\n')
    polaczenie.close()
    text_zak9.configure(state='disabled')

def wyswietl_tabel_zam():
     dsn_t = cx_Oracle.makedsn("localhost", "1521", "orcl")
     polaczenie = cx_Oracle.connect(user="projekt",password="123",dsn=dsn_t)
     test1 = polaczenie.cursor()
     zapytanie = 'select * from projekt.zamowienie'
     test1.execute(zapytanie)
     text_zak9.configure(state='normal')
     text_zak9.delete('1.0', END)
     for row in test1:
        calosc = row[0], '-', row[1], '-' ,row[2] , '-',row[3],'-', row[4],'-',row[5],'-',row[6]
        text_zak9.insert(END, calosc)
        text_zak9.insert(END,'\n')
     polaczenie.close()
     text_zak9.configure(state='disabled')

#okno programu 
okno_glowne = tk.Tk()
okno_glowne.title("Klient biuro turystyczne")
okno_glowne.geometry('420x500')
okno_glowne.wm_iconbitmap("zdjecia\logo.ico")
okno_glowne.resizable(width=False,height=False)

#style  do zakladek
style_zak = ttk.Style()
style_zak.theme_create( "wyglad_zakladek", parent="alt", settings={
        "TNotebook.Tab": {"configure": {"padding": [9,11] },}})
style_zak.theme_use("wyglad_zakladek")

#style do etykiet
style_etykieta  = ttk.Style()
style_etykieta.configure("BW.Label",font=("Courier", 13))

#zakladki
nb = ttk.Notebook(okno_glowne)
zakladka1 = ttk.Frame(nb)
zakladka2 = ttk.Frame(nb)
zakladka3 = ttk.Frame(nb)
zakladka4 = ttk.Frame(nb)
zakladka5 = ttk.Frame(nb)
zakladka6= ttk.Frame(nb)
zakladka7 = ttk.Frame(nb)
zakladka8 = ttk.Frame(nb)
zakladka9 = ttk.Frame(nb)

#dodanie zakledek do widgetu notebook
nb.add(zakladka1, text='Zak1')
nb.add(zakladka2, text='Zak2')
nb.add(zakladka3,text='Zak3')
nb.add(zakladka4,text='Zak4')
nb.add(zakladka5,text='Zak5')
nb.add(zakladka6,text='Zak6')
nb.add(zakladka7,text='Zak7')
nb.add(zakladka8,text='Zak8')
nb.add(zakladka9,text='Zak9')
#wyswietlenie zawartosc widgetu notebook
nb.pack(expand=1,fill="both")

#zakladka 1 obiekty  (funkcja wypisujaca  wszystko o podanym uzytkowniku)
etykieta_klient_imie = ttk.Label(zakladka1,text="klient imie:",style="BW.Label")
etykieta_klient_imie.grid(row=0,column=0,sticky = W, pady = 2);
pole_klient_imie_zak1 = ttk.Entry(zakladka1,width = 30)
pole_klient_imie_zak1.grid(row=0,column=1)

etykieta_klient_nazwisko= ttk.Label(zakladka1,text="klient nazwisko:",style="BW.Label")
etykieta_klient_nazwisko.grid(row=1,column=0,sticky = W, pady = 2);
pole_klient_nazwisko_zak1 = ttk.Entry(zakladka1,width = 30)
pole_klient_nazwisko_zak1.grid(row=1,column=1)

text_zak1=ScrolledText(zakladka1,height = 14,width = 48,state=DISABLED)
text_zak1.grid(row=2 ,column=0,columnspan=2 )

#zak2 wyswietlajaca klientowi  wszystkie wycieczki {zab przed innymi wartosciami niz string } 
#przez podanie kraju i miasta

etykieta_kraj = ttk.Label(zakladka2,text="Kraj:",style="BW.Label")
etykieta_kraj.grid(row=0,column=0,sticky = W, pady = 2)
pole_kraj_zak2 =  ttk.Entry(zakladka2,width = 30)
pole_kraj_zak2.grid(row=0,column=1)

etykieta_miasto =  ttk.Label(zakladka2,text="miasto:",style="BW.Label")
etykieta_miasto.grid(row=1,column=0,sticky = W, pady = 2)
pole_miasto_zak2 =  ttk.Entry(zakladka2,width = 30)
pole_miasto_zak2.grid(row=1,column=1)

text_wycieczki=ScrolledText(zakladka2,height = 14,width = 48,state=DISABLED)
text_wycieczki.grid(row=2 ,column=0,columnspan=2 )

#zak3 dodawanie klienta

etykieta_klient_imie_zak3 =  ttk.Label(zakladka3,text="klient imie:",style="BW.Label")
etykieta_klient_imie_zak3.grid(row=0,column=0,sticky = W, pady = 2)
pole_klient_imie_zak3 =  ttk.Entry(zakladka3,width=30)
pole_klient_imie_zak3.grid(row=0,column=1)

etykieta_klient_nazwisko_zak3 =ttk.Label(zakladka3,text="klient nazwisko:",style="BW.Label")
etykieta_klient_nazwisko_zak3.grid(row=1,column=0,sticky = W, pady = 2)
pole_klient_nazwisko_zak3 = ttk.Entry(zakladka3,width=30)
pole_klient_nazwisko_zak3.grid(row=1,column=1)

etykieta_ilosc_osob_zak3 = ttk.Label(zakladka3,text="ilosc_osob:",style="BW.Label")
etykieta_ilosc_osob_zak3.grid(row=2,column=0,sticky = W, pady = 2)
pole_ilosc_osob_zak3 = ttk.Entry(zakladka3,width=30) 
pole_ilosc_osob_zak3.grid(row=2,column=1)

etykieta_rodzaj_zamowienie_zak3 = ttk.Label(zakladka3,text="rodzaj_zamowienie:",style="BW.Label")
etykieta_rodzaj_zamowienie_zak3.grid(row=3,column=0,sticky = W, pady = 2)
pole_rodzaj_zamowienie_zak3 = ttk.Entry(zakladka3,width=30)
pole_rodzaj_zamowienie_zak3.grid(row=3,column=1)

etykieta_miasto_zak3 =  ttk.Label(zakladka3,text="miasto:",style="BW.Label")
etykieta_miasto_zak3.grid(row=4,column=0,sticky = W, pady = 2)
pole_miasto_zak3 = ttk.Entry(zakladka3,width=30)
pole_miasto_zak3.grid(row=4,column=1)

etykieta_srodek_transportu_zak3 = ttk.Label(zakladka3,text="srodek_transportu:",style="BW.Label")
etykieta_srodek_transportu_zak3.grid(row=5,column=0,sticky = W, pady = 2)
pole_srodek_transportu_zak3 = ttk.Entry(zakladka3,width=30)
pole_srodek_transportu_zak3.grid(row=5,column=1)

text_zak3=ScrolledText(zakladka3,height = 1,width = 48,state=DISABLED)
text_zak3.grid(row=6 ,column=0,columnspan=2 )

#zak4 usuwanie klienta
etykieta_klient_imie =  ttk.Label(zakladka4,text="klient imie:",style="BW.Label")
etykieta_klient_imie.grid(row=0,column=0,sticky = W, pady = 2)
pole_klient_imie_zak4 =  ttk.Entry(zakladka4,width=30)
pole_klient_imie_zak4.grid(row=0,column=1)

etykieta_klient_nazwisko =ttk.Label(zakladka4,text="klient nazwisko:",style="BW.Label")
etykieta_klient_nazwisko.grid(row=1,column=0,sticky = W, pady = 2)
pole_klient_nazwisko_zak4 = ttk.Entry(zakladka4,width=30)
pole_klient_nazwisko_zak4.grid(row=1,column=1)

etykieta_id_zamowienia = ttk.Label(zakladka4,text="id_zamowienia:",style="BW.Label")
etykieta_id_zamowienia.grid(row=2,column=0,sticky = W, pady = 2)
pole_id_zamowienia_zak4 = ttk.Entry(zakladka4,width=30)
pole_id_zamowienia_zak4.grid(row=2,column=1)

text_zak4=ScrolledText(zakladka4,height = 1,width = 48,state=DISABLED)
text_zak4.grid(row=3 ,column=0,columnspan=2 )

#zak5 aktualizacja klienta
etykieta_klient_imie =  ttk.Label(zakladka5,text="klient imie:",style="BW.Label")
etykieta_klient_imie.grid(row=0,column=0,sticky = W, pady = 2)
pole_klient_imie_zak5 =  ttk.Entry(zakladka5,width=30)
pole_klient_imie_zak5.grid(row=0,column=1)

etykieta_klient_nazwisko =ttk.Label(zakladka5,text="klient nazwisko:",style="BW.Label")
etykieta_klient_nazwisko.grid(row=1,column=0,sticky = W, pady = 2)
pole_klient_nazwisko_zak5 = ttk.Entry(zakladka5,width=30)
pole_klient_nazwisko_zak5.grid(row=1,column=1)

etykieta_wpr_id_zamowienia_zak5 = ttk.Label(zakladka5,text="id_zamowienia:",style="BW.Label")
etykieta_wpr_id_zamowienia_zak5.grid(row=2,column=0,sticky = W, pady = 2)
pole_wpr_id_zamowienia_zak5  = ttk.Entry(zakladka5,width=30) 
pole_wpr_id_zamowienia_zak5.grid(row=2,column=1)

etykieta_nowe_imie = ttk.Label(zakladka5,text="nowe imie:",style="BW.Label")
etykieta_nowe_imie.grid(row=3,column=0,sticky = W, pady = 2)
pole_nowe_imie_zak5 = ttk.Entry(zakladka5,width=30) 
pole_nowe_imie_zak5.grid(row=3,column=1)

etykieta_nowe_nazwisko  = ttk.Label(zakladka5,text="nowe_nazwisko:",style="BW.Label")
etykieta_nowe_nazwisko.grid(row=6,column=0,sticky = W, pady = 2)
pole_nowe_nazwisko_zak5  = ttk.Entry(zakladka5,width=30) 
pole_nowe_nazwisko_zak5.grid(row=6,column=1)

etykieta_zmiana_ulgi = ttk.Label(zakladka5,text="rodzaj_zamowienie:",style="BW.Label")
etykieta_zmiana_ulgi.grid(row=7,column=0,sticky = W, pady = 2)
pole_zmiana_ulgi_zak5  = ttk.Entry(zakladka5,width=30)
pole_zmiana_ulgi_zak5.grid(row=7,column=1)

etykieta_nowy_transport = ttk.Label(zakladka5,text="srodek_transportu:",style="BW.Label")
etykieta_nowy_transport.grid(row=8,column=0,sticky = W, pady = 2)
pole_nowy_transport_zak5 = ttk.Entry(zakladka5,width=30)
pole_nowy_transport_zak5.grid(row=8,column=1)

text_zak5=ScrolledText(zakladka5,height = 1,width = 48,state=DISABLED)
text_zak5.grid(row=9 ,column=0,columnspan=2 )

#zak6 procedura wyswietlajaca zestawienie podanego roku   [uzytkownik podaje rok]
#domyslnie jest zwracana zestawienie wszystkich rokow [tylko typ number]
#(suma koszty )
etykieta_zestawienie_roku = ttk.Label(zakladka6,text="zestawienie_roku:",style="BW.Label")
etykieta_zestawienie_roku.grid(row=0,column=0,sticky = W, pady = 2)
pole_zestawienie_roku = ttk.Entry(zakladka6,width=30)
pole_zestawienie_roku.grid(row=0,column=1)
text_zak6=ScrolledText(zakladka6,height = 20,width = 49,state=DISABLED)
text_zak6.grid(row=1 ,column=0,columnspan=2)

#zak7--(zweryfikowac tabele samolot, pociag ,autobus , transport)
#--3)funkcja wyswietlaja wszystkie trasy okreslonego transportu
#--(dziala)
MODES = [
    ("autobus","autobus"),
    ("pociag","pociag"),
    ("samolot","samolot"),
]
ustaw_wartosc = StringVar()
ustaw_wartosc.set("autobus")

for text,mode in MODES:
      transport = Radiobutton(zakladka7, text=text, variable=ustaw_wartosc, value=mode ,width=60,font=("Courier", 15),padx=2)
      transport.pack(anchor=W,expand=1,fill="both")

text_zak7=ScrolledText(zakladka7,height = 15,width = 41,state=DISABLED)
text_zak7.pack(expand=1,fill="both" )

#zakladka 8 wyswietla wartosc do danej ulgi
etykieta_zamowienie_rodzaj = ttk.Label(zakladka8,text="rodzaj ulgi:",style="BW.Label")
etykieta_zamowienie_rodzaj.grid(row=0,column=0,sticky = W, pady = 2)
pole_zamowienie_rodzaj_zak8 = ttk.Entry(zakladka8,width=30)
pole_zamowienie_rodzaj_zak8.grid(row=0,column=1)
text_zak8=ScrolledText(zakladka8,height = 20,width = 48,state=DISABLED)
text_zak8.grid(row=2 ,column=0,columnspan=2 )

#zakladka 9 wyswietla zawartosc dwoch tabeli
text_zak9=ScrolledText(zakladka9,height = 20,width = 48,state=DISABLED)
text_zak9.grid(row=0 ,column=0,columnspan=2 )

#przyciski 
przycisk = Button(zakladka1,text = "Informacje o kliencie",width = 25,command=wyswietl_dane_klienta)
przycisk.grid(row=4,column=0,columnspan=2)

przycisk2 = Button(zakladka2,text = "Wyswietl informacje ",width = 25,command=wyswietl_kraj_miasto)
przycisk2.grid(row=3,column=0,columnspan=2)

przycisk3 = Button(zakladka3,text = "dodaj klienta",width = 25,command=dodawanie_danych_klienta)
przycisk3.grid(row=7,column=0,columnspan=2)

przycisk4 = Button(zakladka4,text = "Usun klienta",width = 25,command=usuwanie_danych_klienta)
przycisk4.grid(row=4,column=0,columnspan=2)

przycisk5 = Button(zakladka5,text="Zaktualizuj dane klienta",width = 25,command=aktualizowanie_danych_klienta)
przycisk5.grid(row=10,column=1)

przycisk6 = Button(zakladka6,text="Wyswietl zestawienie roku",width = 25,command=zestawienie_roku)
przycisk6.grid(row=2,column=0,columnspan=2)

przycisk7 = Button(zakladka7,text="Wyswietl przejazdy danego  transportu",command=rodzaj_transportu)
przycisk7.pack(expand=1,fill="both" )

przycisk8 = Button(zakladka8,text="Wyswietl suma uwzgledniajac ulgi",command=suma_z_ulga)
przycisk8.grid(row=3,column=0,columnspan=2 )

przycisk9_1 = Button(zakladka9,text="Wyswietl tabele klient",width = 25,command=wyswietl_tabel_kl)
przycisk9_1.grid(row=1,column=0)

przycisk9_2 = Button(zakladka9,text="Wyswietl tabele zamowienie",width = 25,command=wyswietl_tabel_zam)
przycisk9_2.grid(row=1,column=1)

okno_glowne.mainloop() 
