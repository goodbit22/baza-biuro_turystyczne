Create sequence adres_seq
Start with 1
increment by 1
maxvalue 99999
nocycle
noorder
;

Create sequence atrakcje_seq
Start with 1
increment by 1
maxvalue 99999
nocycle
noorder
;

create sequence autobus_seq
Start with 1
increment by 1
maxvalue 99999
nocycle
noorder;

create sequence klient_seq
Start with 1
increment by 1
maxvalue 99999
nocycle
noorder;


create sequence pociag_seq
Start with 1
increment by 1
maxvalue 99999
nocycle
noorder;

create sequence samolot_seq
Start with 1
increment by 1
maxvalue 99999
nocycle
noorder
;


create sequence transport_seq
Start with 1
increment by 1
maxvalue 99999
nocycle
noorder
;

create sequence wyjazd_seq
Start with 1
increment by 1
maxvalue 99999
nocycle
noorder
;
create sequence zakwaterowanie_seq
Start with 1
increment by 1
minvalue 0
maxvalue 99999
nocycle
noorder;

create sequence zamowienie_seq
Start with 1
increment by 1
maxvalue 99999
nocycle
noorder;

Insert into Adres(Id_adresu,Miasto,Ulica,numer_lokalu,numer_budynku) Values(adres_seq.nextval,'Moskwa','Carska',1,22);
Insert into Adres(Id_adresu,Miasto,Ulica,numer_lokalu,numer_budynku) Values(adres_seq.nextval,'Petersburg','Krasnolyjska',22,23);
Insert into Adres(Id_adresu,Miasto,Ulica,numer_lokalu,numer_budynku) Values(adres_seq.nextval,'Bruksela','Wolnosc',11,33);
Insert into Adres(Id_adresu,Miasto,Ulica,numer_lokalu,numer_budynku) Values(adres_seq.nextval,'Paryz','Kwiatowa',13,15);
Insert into Adres(Id_adresu,Miasto,Ulica,numer_lokalu,numer_budynku) Values(adres_seq.nextval,'Berlin','Murowana',31,33);
Insert into Adres(Id_adresu,Miasto,Ulica,numer_lokalu,numer_budynku) Values(adres_seq.nextval,'Amsterdam','Holenderska',91,13);
Insert into Adres(Id_adresu,Miasto,Ulica,numer_lokalu,numer_budynku) Values(adres_seq.nextval,'Barcelona','Hiszpanska',53,31);
Insert into Adres(Id_adresu,Miasto,Ulica,numer_lokalu,numer_budynku) Values(adres_seq.nextval,'Wieden','Górska',23,13);
Insert into Adres(Id_adresu,Miasto,Ulica,numer_lokalu,numer_budynku) Values(adres_seq.nextval,'Londyn','Herbaciana',11,66);
Insert into Adres(Id_adresu,Miasto,Ulica,numer_lokalu,numer_budynku) Values(adres_seq.nextval,'Wilno','Swobody',11,13);
commit;


Insert Into Wyjazd(Id_wyjazd,Kraj,Miejscowosc,Czas_trwania,Cena_wycieczki,Cena_za_osobe) VALUES(wyjazd_seq.nextval,'Rosja','Moskwa',3,2000,90);
Insert Into Wyjazd(Id_wyjazd,Kraj,Miejscowosc,Czas_trwania,Cena_wycieczki,Cena_za_osobe) VALUES(wyjazd_seq.nextval,'Rosja','Petersburg',4,2100,120);
Insert Into Wyjazd(Id_wyjazd,Kraj,Miejscowosc,Czas_trwania,Cena_wycieczki,Cena_za_osobe) VALUES(wyjazd_seq.nextval,'Belgia','Bruksela',3,2500,200);
Insert Into Wyjazd(Id_wyjazd,Kraj,Miejscowosc,Czas_trwania,Cena_wycieczki,Cena_za_osobe) VALUES(wyjazd_seq.nextval,'Francja','Paryz',2,3000,100);
Insert Into Wyjazd(Id_wyjazd,Kraj,Miejscowosc,Czas_trwania,Cena_wycieczki,Cena_za_osobe) VALUES(wyjazd_seq.nextval,'Niemcy','Berlin',4,1700,140);
Insert Into Wyjazd(Id_wyjazd,Kraj,Miejscowosc,Czas_trwania,Cena_wycieczki,Cena_za_osobe) VALUES(wyjazd_seq.nextval,'Holandia','Amsterdam',5,2460,150);
Insert Into Wyjazd(Id_wyjazd,Kraj,Miejscowosc,Czas_trwania,Cena_wycieczki,Cena_za_osobe) VALUES(wyjazd_seq.nextval,'Hiszpania','Barcelona',7,2500,130);
Insert Into Wyjazd(Id_wyjazd,Kraj,Miejscowosc,Czas_trwania,Cena_wycieczki,Cena_za_osobe) VALUES(wyjazd_seq.nextval,'Litwa','Wilno',11,1900,110);
Insert Into Wyjazd(Id_wyjazd,Kraj,Miejscowosc,Czas_trwania,Cena_wycieczki,Cena_za_osobe) VALUES(wyjazd_seq.nextval,'Wielka Brytania','Londyn',12,2340,135);
Insert Into Wyjazd(Id_wyjazd,Kraj,Miejscowosc,Czas_trwania,Cena_wycieczki,Cena_za_osobe) VALUES(wyjazd_seq.nextval,'Austria','Wieden',3,3500,160);
commit;


Insert Into Atrakcje VALUES(atrakcje_seq.nextval,1,'zwiedzanie Placu Czerwonego','21:15','13:00',150);
Insert Into Atrakcje VALUES(atrakcje_seq.nextval,1,'zwiedzanie Mauzoleum Lenina','19:55','15:45',210 );
Insert Into Atrakcje VALUES(atrakcje_seq.nextval,1,'zwiedzanie Kreml moskiewski','18:00','12:00',200.5 );
Insert Into Atrakcje VALUES(atrakcje_seq.nextval,3,'zwiedzanie Atomium','17:00','11:00',250);
Insert Into Atrakcje VALUES(atrakcje_seq.nextval,4,'zwiedzanie Wiezy Eiffla','20:15','15:00',150);
Insert Into Atrakcje VALUES(atrakcje_seq.nextval,2,'zwiedzanie Krazownika Aurora','16:30','14:15',160);
Insert Into Atrakcje VALUES(atrakcje_seq.nextval,4,'zwiedzanie Luwru','17:00','13:00',180);
Insert Into Atrakcje VALUES(atrakcje_seq.nextval,5,'zwiedzanie Alexanderplatz','16:15','11:30',150);
Insert Into Atrakcje VALUES(atrakcje_seq.nextval,5,'zwiedzanie Reichstag','17:00','12:15',100);
Insert Into Atrakcje VALUES(atrakcje_seq.nextval,2,'zwiedzanie Newski Prospekt','15:30','11:30',200);
commit;



INSERT INTO Transport(Id_transport,id_wyjazdu,Rodzaj_transportu) VALUES(transport_seq.nextval,6,'autobus');
INSERT INTO Transport(Id_transport,id_wyjazdu,Rodzaj_transportu) VALUES(transport_seq.nextval,5,'samolot');
INSERT INTO Transport(Id_transport,id_wyjazdu,Rodzaj_transportu) VALUES(transport_seq.nextval,3,'autobus');
INSERT INTO Transport(Id_transport,id_wyjazdu,Rodzaj_transportu) VALUES(transport_seq.nextval,3,'pociag');
INSERT INTO Transport(Id_transport,id_wyjazdu,Rodzaj_transportu) VALUES(transport_seq.nextval,5,'autobus');
INSERT INTO Transport(Id_transport,id_wyjazdu,Rodzaj_transportu) VALUES(transport_seq.nextval,6,'samolot');
INSERT INTO Transport(Id_transport,id_wyjazdu,Rodzaj_transportu) VALUES(transport_seq.nextval,4,'pociag');
INSERT INTO Transport(Id_transport,id_wyjazdu,Rodzaj_transportu) VALUES(transport_seq.nextval,1,'autobus');
INSERT INTO Transport(Id_transport,id_wyjazdu,Rodzaj_transportu) VALUES(transport_seq.nextval,7,'samolot');
INSERT INTO Transport(Id_transport,id_wyjazdu,Rodzaj_transportu) VALUES(transport_seq.nextval,2,'pociag');
commit;


Insert Into Autobus(id_autobusu,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(autobus_seq.nextval,1,'Gdansk','Berlin',200);
Insert Into Autobus(id_autobusu,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(autobus_seq.nextval,3,'Warszawa','Paryz',300);
Insert Into Autobus(id_autobusu,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(autobus_seq.nextval,5,'Krakow','Wilno',200);
Insert Into Autobus(id_autobusu,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(autobus_seq.nextval,8,'Katowice','Moskwa',250);
Insert Into Autobus(id_autobusu,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(autobus_seq.nextval,3,'Warszawa','Petersburg',280);
Insert Into Autobus(id_autobusu,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(autobus_seq.nextval,5,'Krakow','Wieden',170);
Insert Into Autobus(id_autobusu,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(autobus_seq.nextval,1,'Gdansk','Amsterdam',330);
Insert Into Autobus(id_autobusu,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(autobus_seq.nextval,8,'Katowice','Bruksela',250);
Insert Into Autobus(id_autobusu,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(autobus_seq.nextval,8,'Katowice','Berlin',180);
Insert Into Autobus(id_autobusu,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(autobus_seq.nextval,1,'Gdansk','Barcelona',190);
commit;


Insert Into Klient(id_klienta,imie,nazwisko) Values(klient_seq.nextval,'Ola','Kowalska');
Insert Into Klient(id_klienta,imie,nazwisko) Values(klient_seq.nextval,'Pawel','Nowak');
Insert Into Klient(id_klienta,imie,nazwisko) Values(klient_seq.nextval,'Daniel','Kwiatkowski');
Insert Into Klient(id_klienta,imie,nazwisko) Values(klient_seq.nextval,'Olaf','Adamowicz');
Insert Into Klient(id_klienta,imie,nazwisko) Values(klient_seq.nextval,'Janusz','Antos');
Insert Into Klient(id_klienta,imie,nazwisko) Values(klient_seq.nextval,'Wiktor','Bartyzel');
Insert Into Klient(id_klienta,imie,nazwisko) Values(klient_seq.nextval,'Wiktoria','Gajda');
Insert Into Klient(id_klienta,imie,nazwisko) Values(klient_seq.nextval,'Arkadiusz','Kowalska');
Insert Into Klient(id_klienta,imie,nazwisko) Values(klient_seq.nextval,'Filip','Fikus');
Insert Into Klient(id_klienta,imie,nazwisko) Values(klient_seq.nextval,'Bartek','Boruc');
commit;


Insert Into Pociag(id_pociag,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(pociag_seq.nextval,4,'Katowice','Bruksela',300);
Insert Into Pociag(id_pociag,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(pociag_seq.nextval,7,'Krakow','Petersburg',399);
Insert Into Pociag(id_pociag,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(pociag_seq.nextval,10,'Warszawa','Moskwa',310);
Insert Into Pociag(id_pociag,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(pociag_seq.nextval,10,'Warszawa','Berlin',250);
Insert Into Pociag(id_pociag,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(pociag_seq.nextval,10,'Warszawa','Amsterdam',110);
Insert Into Pociag(id_pociag,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(pociag_seq.nextval,10,'Warszawa','Petersburg',400);
Insert Into Pociag(id_pociag,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(pociag_seq.nextval,7,'Krakow','Wilno',220);
Insert Into Pociag(id_pociag,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(pociag_seq.nextval,7,'Krakow','Barcelona',400);
Insert Into Pociag(id_pociag,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(pociag_seq.nextval,7,'Krakow','Paryz',350);
Insert Into Pociag(id_pociag,id_transport,skad_wyjezdza,dokad_jedzie,cena_przejazdu) Values(pociag_seq.nextval,4,'Katowice','Wieden',300);
commit;



Insert Into Samolot(id_samolot,id_transport,skad_wylatuje,dokad_jedzie,cena_przelotu) Values(samolot_seq.nextval,2,'Wroclaw','Wieden',300);
Insert Into Samolot(id_samolot,id_transport,skad_wylatuje,dokad_jedzie,cena_przelotu) Values(samolot_seq.nextval,6,'Krakow','Amsterdam',250);
Insert Into Samolot(id_samolot,id_transport,skad_wylatuje,dokad_jedzie,cena_przelotu) Values(samolot_seq.nextval,9,'Warszawa','Moskwa',360);
Insert Into Samolot(id_samolot,id_transport,skad_wylatuje,dokad_jedzie,cena_przelotu) Values(samolot_seq.nextval,9,'Warszawa','Berlin',220);
Insert Into Samolot(id_samolot,id_transport,skad_wylatuje,dokad_jedzie,cena_przelotu) Values(samolot_seq.nextval,9,'Warszawa','Paryz',300);
Insert Into Samolot(id_samolot,id_transport,skad_wylatuje,dokad_jedzie,cena_przelotu) Values(samolot_seq.nextval,9,'Warszawa','Barcelona',400);
Insert Into Samolot(id_samolot,id_transport,skad_wylatuje,dokad_jedzie,cena_przelotu) Values(samolot_seq.nextval,6,'Krakow','Brusksela',180);
Insert Into Samolot(id_samolot,id_transport,skad_wylatuje,dokad_jedzie,cena_przelotu) Values(samolot_seq.nextval,6,'Krakow','Petersburg',190);
Insert Into Samolot(id_samolot,id_transport,skad_wylatuje,dokad_jedzie,cena_przelotu) Values(samolot_seq.nextval,2,'Wroclaw','Berlin',220);
Insert Into Samolot(id_samolot,id_transport,skad_wylatuje,dokad_jedzie,cena_przelotu) Values(samolot_seq.nextval,6,'Krakow','Wilno',320);
commit;


INSERT INTO Zakwaterowanie(Id_zakwaterowanie,id_wyjazdu,id_adres,nazwa_zakwaterowania,jakosc_opcji,cena_za_dobe,liczba_pokoj) VALUES(zakwaterowanie_seq.nextval,zakwaterowanie_seq.currval,zakwaterowanie_seq.currval,'Blooms Inn Apartment',3,70,20);
INSERT INTO Zakwaterowanie(Id_zakwaterowanie,id_wyjazdu,id_adres,nazwa_zakwaterowania,jakosc_opcji,cena_za_dobe,liczba_pokoj) VALUES(zakwaterowanie_seq.nextval,zakwaterowanie_seq.currval,zakwaterowanie_seq.currval,'Apple Park',4,40,30);
INSERT INTO Zakwaterowanie(Id_zakwaterowanie,id_wyjazdu,id_adres,nazwa_zakwaterowania,jakosc_opcji,cena_za_dobe,liczba_pokoj) VALUES(zakwaterowanie_seq.nextval,zakwaterowanie_seq.currval,zakwaterowanie_seq.currval,'Camping Malta',5,100,25);
INSERT INTO Zakwaterowanie(Id_zakwaterowanie,id_wyjazdu,id_adres,nazwa_zakwaterowania,jakosc_opcji,cena_za_dobe,liczba_pokoj) VALUES(zakwaterowanie_seq.nextval,zakwaterowanie_seq.currval,zakwaterowanie_seq.currval,'Lemon Plaza Hotel',5,90,40);
INSERT INTO Zakwaterowanie(Id_zakwaterowanie,id_wyjazdu,id_adres,nazwa_zakwaterowania,jakosc_opcji,cena_za_dobe,liczba_pokoj) VALUES(zakwaterowanie_seq.nextval,zakwaterowanie_seq.currval,zakwaterowanie_seq.currval,'IBB Andersia Hotel',3,50,32);
INSERT INTO Zakwaterowanie(Id_zakwaterowanie,id_wyjazdu,id_adres,nazwa_zakwaterowania,jakosc_opcji,cena_za_dobe,liczba_pokoj) VALUES(zakwaterowanie_seq.nextval,zakwaterowanie_seq.currval,zakwaterowanie_seq.currval,'FOLK Hostel ',4,80,18);
INSERT INTO Zakwaterowanie(Id_zakwaterowanie,id_wyjazdu,id_adres,nazwa_zakwaterowania,jakosc_opcji,cena_za_dobe,liczba_pokoj) VALUES(zakwaterowanie_seq.nextval,zakwaterowanie_seq.currval,zakwaterowanie_seq.currval,'Retro Hostel ',3,75,25);
INSERT INTO Zakwaterowanie(Id_zakwaterowanie,id_wyjazdu,id_adres,nazwa_zakwaterowania,jakosc_opcji,cena_za_dobe,liczba_pokoj) VALUES(zakwaterowanie_seq.nextval,zakwaterowanie_seq.currval,10,'Apartamenty Velvet',5,45,17);
INSERT INTO Zakwaterowanie(Id_zakwaterowanie,id_wyjazdu,id_adres,nazwa_zakwaterowania,jakosc_opcji,cena_za_dobe,liczba_pokoj) VALUES(zakwaterowanie_seq.nextval,zakwaterowanie_seq.currval,zakwaterowanie_seq.currval,'Moon Hostel ',4,67,20);
INSERT INTO Zakwaterowanie(Id_zakwaterowanie,id_wyjazdu,id_adres,nazwa_zakwaterowania,jakosc_opcji,cena_za_dobe,liczba_pokoj) VALUES(zakwaterowanie_seq.nextval,zakwaterowanie_seq.currval,8,'Hotel Meridian',4,80,15);
commit;


Insert into Zamowienie(Id_zamowienia,id_wyjazdu,id_klienta,ilosc_osob,Data_platnosc,rodzaj_zamowienia,transport) VALUES (zamowienie_seq.nextval,zamowienie_seq.currval,1,2,TO_DATE('05-Lut-2018 ','DD-MON-YYYY'),'seniorski','samolot');
Insert into Zamowienie(Id_zamowienia,id_wyjazdu,id_klienta,ilosc_osob,Data_platnosc,rodzaj_zamowienia,transport) VALUES (zamowienie_seq.nextval,zamowienie_seq.currval,2,4,TO_DATE('05-Lut-2018 ','DD-MON-YYYY'),'rodzinny','autobus');
Insert into Zamowienie(Id_zamowienia,id_wyjazdu,id_klienta,ilosc_osob,Data_platnosc,rodzaj_zamowienia,transport) VALUES (zamowienie_seq.nextval,zamowienie_seq.currval,2,2,TO_DATE('06-Lut-2018 ','DD-MON-YYYY'),'dla par','pociag');
Insert into Zamowienie(Id_zamowienia,id_wyjazdu,id_klienta,ilosc_osob,Data_platnosc,rodzaj_zamowienia,transport) VALUES (zamowienie_seq.nextval,zamowienie_seq.currval,4,1,TO_DATE('07-Lut-2018 ','DD-MON-YYYY'),'normalny','pociag');
Insert into Zamowienie(Id_zamowienia,id_wyjazdu,id_klienta,ilosc_osob,Data_platnosc,rodzaj_zamowienia,transport) VALUES (zamowienie_seq.nextval,zamowienie_seq.currval,5,5,TO_DATE('05-STY-2018 ','DD-MON-YYYY'),'firmowy','pociag');
Insert into Zamowienie(Id_zamowienia,id_wyjazdu,id_klienta,ilosc_osob,Data_platnosc,rodzaj_zamowienia,transport) VALUES (zamowienie_seq.nextval,zamowienie_seq.currval,9,5,TO_DATE('05-LIS-2019 ','DD-MON-YYYY'),'firmowy','autobus');
Insert into Zamowienie(Id_zamowienia,id_wyjazdu,id_klienta,ilosc_osob,Data_platnosc,rodzaj_zamowienia,transport) VALUES (zamowienie_seq.nextval,zamowienie_seq.currval,10,2,TO_DATE('05-GRU-2019 ','DD-MON-YYYY'),'normalny','samolot');
Insert into Zamowienie(Id_zamowienia,id_wyjazdu,id_klienta,ilosc_osob,Data_platnosc,rodzaj_zamowienia,transport) VALUES (zamowienie_seq.nextval,zamowienie_seq.currval,1,3,TO_DATE('05-Lut-2018 ','DD-MON-YYYY'),'seniorski','samolot');
Insert into Zamowienie(Id_zamowienia,id_wyjazdu,id_klienta,ilosc_osob,Data_platnosc,rodzaj_zamowienia,transport) VALUES (zamowienie_seq.nextval,zamowienie_seq.currval,3,7,TO_DATE('05-Maj-2019 ','DD-MON-YYYY'),'firmowy','samolot');
Insert into Zamowienie(Id_zamowienia,id_wyjazdu,id_klienta,ilosc_osob,Data_platnosc,rodzaj_zamowienia,transport) VALUES (zamowienie_seq.nextval,zamowienie_seq.currval,2,3,TO_DATE('06-Lut-2018 ','DD-MON-YYYY'),'rodzinny','autobus');
commit;


