--tworzenie kolumny
alter table  atrakcje 
add test varchar2(20) ;

-- dodawanie ogranicenie not null
alter table atrakcje
modify (test varchar2(20) constraint atrakcje_nn not null);

--usuwanie ograniczenia not null
alter table atrakcje 
drop constraint atrakcje_nn;

--usuwanie kolumny test
alter table atrakcje
drop column test;



