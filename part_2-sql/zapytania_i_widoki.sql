--zapytania
--1)dotyczaca wybranie z tabeli autobu celow jazdy zaczynajacych sie na B i wyswiwtlanie o nich dane   (dziala)  
Select adr.miasto ,adr.ulica  , adr.numer_lokalu , adr.numer_budynku 
,zak.jakosc_opcji , zak.cena_za_dobe , zak.liczba_pokoj,zak.nazwa_zakwaterowania zakwaterowanie
From adres adr,zakwaterowanie zak , wyjazd wyj 
Where adr.id_adresu = zak.id_adres AND zak.id_wyjazdu = wyj.id_wyjazd
AND adr.miasto IN(select DISTINCT dokad_jedzie from autobus where dokad_jedzie Like 'B%')  ;

--2)wyswietlic najdrozszy i najtanszy  hotel w jakim miescie i kraju  (dziala) poprawione 

Select 'najdrozszy hotel: '   ||  wyj.kraj , ad.miasto , zak.nazwa_zakwaterowania zakwaterowanie ,zak.jakosc_opcji,zak.cena_za_dobe cena_doba 
From zakwaterowanie zak , adres ad , wyjazd wyj
Where zak.id_adres = ad.id_adresu
AND zak.id_wyjazdu = wyj.id_wyjazd
AND zak.cena_za_dobe = (select MAX(cena_za_dobe) From zakwaterowanie)
UNION 
Select 'najtanszy hotel: '|| wyj.kraj , ad.miasto , zak.nazwa_zakwaterowania zakwaterowanie ,zak.jakosc_opcji,zak.cena_za_dobe cena_doba
From zakwaterowanie zak , adres ad , wyjazd wyj
Where zak.id_adres = ad.id_adresu
AND zak.id_wyjazdu = wyj.id_wyjazd
AND zak.cena_za_dobe = (select MIN(cena_za_dobe) From zakwaterowanie);

--3) wyswietlic osoby ktore zarezerwowany wycieczki dla osob pomiedy srednia iloscia osob  a  wartoscia maksymalna  (dziala) 
select kl.imie , kl.nazwisko , zam.ilosc_osob , zam.rodzaj_zamowienia  
from zamowienie zam , klient kl
where zam.id_klienta = kl.id_klienta
AND ilosc_osob Between (Select AVG(ilosc_osob) FROM zamowienie) AND (select MAX(ilosc_osob) from zamowienie);

--4)miasto z najwieksza ilosc atrakcji i wyswietla dla niego trasy srodkami transportu samolotem i autobusem  (dziala) 
select 'autobus: '|| aut.skad_wyjezdza || ' - ' ||  aut.dokad_jedzie  trasy_sam_i_aut   FROM autobus  aut Where aut.dokad_jedzie = (Select miejscowosc  miasto_z_naj_atrakcji FROM wyjazd  Where wyjazd.id_wyjazd =  (Select id_wyjazdu  wyjazd FROM 
Atrakcje GROUP  BY id_wyjazdu HAVING COUNT(id_atrakcji) = MAX(id_atrakcji)))
Union
select 'samolot: ' || sam.skad_wylatuje || ' - ' || sam.dokad_jedzie   FROM samolot  sam Where sam.dokad_jedzie = (Select miejscowosc  miasto_z_naj_atrakcji FROM wyjazd  Where wyjazd.id_wyjazd =  (Select id_wyjazdu  wyjazd FROM 
Atrakcje GROUP  BY id_wyjazdu HAVING COUNT(id_atrakcji) = MAX(id_atrakcji)));


--5)wyswietlic   imie  nazwisko   osoby , nazwe hotele ,nazwa miasto i kraj  ktore cena jest wieksza od sredniej cena_za_odobe
--ktore zostaje zmniejsza przez wartosc min  cena_za_dobe
--i jakosc opcji jest wieksza od wprowadzonej_przez_uzytkownika (dziala) 
select wyj.id_wyjazd ,kl.imie,kl.nazwisko,
wyj.kraj ,wyj.miejscowosc , zak.nazwa_zakwaterowania 
FROM zakwaterowanie zak,wyjazd wyj ,zamowienie zam , klient kl where 
zak.id_wyjazdu = wyj.id_wyjazd 
and wyj.id_wyjazd = zam.id_wyjazdu
and zam.id_klienta = kl.id_klienta
and zak.id_zakwaterowanie IN(select  zak.id_zakwaterowanie FROM zakwaterowanie zak,wyjazd wyj
where wyj.id_wyjazd = zak.id_wyjazdu AND zak.jakosc_opcji > '&uzytkownik_jakosc_opcji' )
and wyj.cena_za_osobe > (Select AVG(wyj.cena_za_osobe) FROM wyjazd wyj )-(Select MIN(zak.cena_za_dobe) FROM wyjazd wyj);

--6)wyświetl  miesiac z ostaniej platnosc oraz pore roku jesli jest grudzien zamiast pory roku wyswietla nazewe grudzien  (dziala)
 select kl.imie ,kl.nazwisko  ,  zam.rodzaj_zamowienia , 
    wyj.cena_za_osobe * zam.ilosc_osob + wyj.cena_wycieczki +(wyj.czas_trwania * zak.cena_za_dobe)  calkowita_cena
    ,EXTRACT (MONTH FROM DATA_platnosc) miesiac,
    Case 
        When EXTRACT (MONTH FROM DATA_platnosc) = 12 then 
        'grudzien'
        when EXTRACT (MONTH FROM DATA_platnosc) < 12 AND EXTRACT (MONTH FROM DATA_platnosc) >8  then
        'jesien'
        when EXTRACT (MONTH FROM DATA_platnosc) < 9 AND EXTRACT (MONTH FROM DATA_platnosc) > 5  then
        'lato'
        when EXTRACT (MONTH FROM DATA_platnosc) < 6 AND EXTRACT (MONTH FROM DATA_platnosc) > 2  then
        'wiosna'
        else
            'zima'   
    end
  from zamowienie zam , wyjazd wyj , klient kl , zakwaterowanie zak
    where zam.id_wyjazdu = wyj.id_wyjazd  
    AND zam.id_klienta = kl.id_klienta
    AND  zak.id_wyjazdu = wyj.id_wyjazd
    AND  zam.data_platnosc = (SELECT MAX(zamowienie.data_platnosc) FROM zamowienie);
    


--7)wyswietlic najtansze trasy  we wszystkich srodkach transportu (dziala) 
 select trasy_pociag , trasy_autobus , trasy_samolot FROM  ( select skad_wyjezdza ||' - '|| dokad_jedzie trasy_pociag   
 from pociag  where cena_przejazdu = (Select Min(cena_przejazdu) FROM pociag ))
 ,(select skad_wyjezdza ||' - '|| dokad_jedzie trasy_autobus from 
 autobus where autobus.cena_przejazdu = (Select Min(cena_przejazdu) FROM autobus ))
 ,(select samolot.skad_wylatuje ||' - '|| samolot.dokad_jedzie trasy_samolot from 
 samolot where samolot.cena_przelotu = (Select Min(cena_przelotu) FROM samolot ));
 
 
--8)Wyswietlic osoby ktore ilosc  zamowila byly powyzej sredniej (dziala)
Select UPPER(imie) || ' ' || Lower(nazwisko) as klient From klient Where id_klienta IN (
Select id_klienta  FROM zamowienie group by id_klienta having count(id_zamowienia) > MIN(id_zamowienia));

--9)wyswietl atrakcje ktore znajduja sie w okreslonym kraju (dziala) 
Select wyj.miejscowosc , atr.nazwa_atrakcji ,atr.godzina_wyjscia wyjsc , atr.godzina_przyjscia przyjsc , atr.cena_atrakcji
From wyjazd wyj , atrakcje atr where  atr.id_wyjazdu = wyj.id_wyjazd
AND kraj = '&wybrany_kraj';
-----------------widoki

--10)suma ceny klient z zamoweiniem normlanym(bez uzwglednienia ceny-transportu)(dziala) 
create view normalny as
    select sum(calkowita_cena)  zarobek_z_zamowien_normalnych FROM ( 
        select wyj.cena_za_osobe * zam.ilosc_osob + wyj.cena_wycieczki +(wyj.czas_trwania * zak.cena_za_dobe)  calkowita_cena
        from zamowienie zam , wyjazd wyj , klient kl , zakwaterowanie zak
        where zam.id_wyjazdu = wyj.id_wyjazd  
        AND zam.id_klienta = kl.id_klienta
        AND  zak.id_wyjazdu = wyj.id_wyjazd
        AND zam.rodzaj_zamowienia = 'normalny'
    );    
drop view normalny;

--11)dla emulatorow i odjac 20 % od calkowitej ceny wycieczki (bez uzwglednienia ceny-transportu) (dziala) 
create view cena_emerytow AS
    select kl.imie ,kl.nazwisko  ,
    wyj.cena_za_osobe * zam.ilosc_osob + wyj.cena_wycieczki +(wyj.czas_trwania * zak.cena_za_dobe) as calkowita_cena,
    ((wyj.cena_za_osobe * zam.ilosc_osob + wyj.cena_wycieczki +(wyj.czas_trwania * zak.cena_za_dobe))/10)*8 as calkowita_cena_po_obnizce
    from zamowienie zam , wyjazd wyj , klient kl , zakwaterowanie zak
    where zam.id_wyjazdu = wyj.id_wyjazd  
    AND zam.id_klienta = kl.id_klienta
    AND  zak.id_wyjazdu = wyj.id_wyjazd
    AND zam.rodzaj_zamowienia = 'seniorski' ;
    
Drop view cena_emerytow;
    
    
--12)zestawienie roku 2018 (bez uzwglednienia ceny-transportu) (dziala) 
create view rok_2018 as
    select kl.imie ,kl.nazwisko  ,  zam.rodzaj_zamowienia
    ,wyj.cena_za_osobe * zam.ilosc_osob + wyj.cena_wycieczki +(wyj.czas_trwania * zak.cena_za_dobe)  calkowita_cena
    ,zam.data_platnosc platnosc  from zamowienie zam , wyjazd wyj , klient kl , zakwaterowanie zak
    where zam.id_wyjazdu = wyj.id_wyjazd  
    AND zam.id_klienta = kl.id_klienta
    AND  zak.id_wyjazdu = wyj.id_wyjazd
    AND  zam.data_platnosc > TO_DATE('31-12-17', 'DD-MM-YY')
    AND zam.data_platnosc < TO_DATE('01-01-19', 'DD-MM-YY');
    
drop view rok_2018;

