create table tester_insertow (
    id_tester number(15,0) not null constraint id_tester_pk Primary key,
    id_atrakcji_obcy number(15,0),
    samochod varchar(20),
    ilosc number(3,0)
);
--ograniczenie not null
alter table tester_insertow
modify samochod not null;
insert into tester_insertow Values(1,2,'',2);

--ograniczenie unique
alter table tester_insertow
add unique(samochod);
insert into tester_insertow Values(1,2,'sss',2);
insert into tester_insertow Values(2,2,'sss',2);

delete from tester_insertow;

--ograniczenie primary key
insert into tester_insertow Values(1,2,'sss',2);
insert into tester_insertow Values(1,2,'www',2);


delete from tester_insertow;

--ograniczenie check
alter table tester_insertow
add Check ( ilosc > 2);
insert into tester_insertow Values(1,2,'www',2);

delete from tester_insertow;

--ograniczenie klucza obcego




Drop table tester_insertow CASCADE CONSTRAINTS;