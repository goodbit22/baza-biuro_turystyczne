Delete FROM Adres;
Drop sequence adres_seq;

Delete From atrakcje;
Drop sequence atrakcje_seq;

Delete From Wyjazd;
Drop sequence wyjazd_seq;

Delete From autobus;
drop sequence  autobus_seq;

Delete From pociag;
drop sequence  pociag_seq;

Delete From samolot;
drop sequence  samolot_seq;


Delete From Transport;
drop sequence transport_seq;


Delete From klient;
drop sequence  klient_seq;


Delete From zakwaterowanie;
drop sequence  zakwaterowanie_seq;

Delete From Zamowienie;
drop sequence zamowienie_seq;

commit;