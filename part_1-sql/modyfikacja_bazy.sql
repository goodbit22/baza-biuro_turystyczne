Alter Table Adres Add adres_testowany varchar2(20);
Alter Table Atrakcje ADD atrakcje_test varchar2(10);
Alter Table Autobus ADD aut varchar2(1);
Alter Table Klient ADD kli varchar2(22);
Alter Table Pociag ADD poc varchar2(1);
Alter Table Samolot ADD sam varchar2(1);
Alter Table Transport ADD testowa_trans varchar(22);
Alter Table Wyjazd ADD wyjazd_testowy varchar2(22);
Alter Table Zakwaterowanie ADD zakwa varchar2(20);
Alter Table Zamowienie  ADD zam varchar2(22);


Alter Table Adres MODIFY adres_testowany number(15);
Alter Table Atrakcje MODIFY atrakcje_test number(15);
Alter Table Autobus MODIFY aut varchar2(10);
Alter Table Klient MODIFY kli varchar2(11);
Alter Table Pociag MODIFY poc varchar2(2);
Alter Table Samolot MODIFY sam number(1);
Alter Table Transport MODIFY testowa_trans  number(1);
Alter Table Wyjazd MODIFY wyjazd_testowy number(11);
Alter Table Zakwaterowanie MODIFY zakwa number(11);
Alter Table Zamowienie  MODIFY zam number(1);


Alter Table Adres DROP COLUMN adres_testowany;
Alter Table Atrakcje DROP COLUMN atrakcje_test;
Alter Table Autobus DROP COLUMN aut;
Alter Table Klient DROP COLUMN kli;
Alter Table Pociag DROP COLUMN poc;
Alter Table Samolot DROP COLUMN sam;
Alter Table Transport DROP COLUMN testowa_trans;
Alter Table Wyjazd DROP COLUMN wyjazd_testowy;
Alter Table Zakwaterowanie DROP COLUMN zakwa;
Alter Table Zamowienie DROP COLUMN zam;
