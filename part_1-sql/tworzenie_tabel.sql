
Create Table Wyjazd(
    Id_wyjazd number(15,0)  NOT NULL CONSTRAINT wyjazd_pk PRIMARY KEY,
    Kraj varchar2(20) NOT NULL,
    Miejscowosc varchar2(20) NOT NULL,
    czas_trwania number(3,0) NOT NULL,
    Cena_wycieczki number(6,2) NOT NULL,
    Cena_za_osobe number(5,2) NOT NULL
);

Create Table Atrakcje(
    Id_atrakcji number(15,0) NOT NULL CONSTRAINT atrakcje_pk PRIMARY Key,
    Id_wyjazdu number(15,0) NOT NULL CONSTRAINT wyjazd_atrakcje_fk REFERENCES Wyjazd(Id_wyjazd),
    Nazwa_atrakcji varchar2(30) NOT  NULL ,
    Godzina_przyjscia varchar2(5) NOT NULL,
    Godzina_wyjscia varchar2(5)  NOT NULL,
    Cena_atrakcji number(5,2) NOT NULL
);

CREATE TABLE Klient(
	Id_klienta number(9,0) NOT NULL CONSTRAINT klient_pk PRIMARY KEY,
	Imie varchar2(20) NOT NULL,
	Nazwisko varchar2(20) NOT NULL
);

Create Table Zamowienie(
    Id_zamowienia number(15,0) NOT NULL CONSTRAINT zamowienie_pk PRIMARY KEY,
    Id_wyjazdu  number(15,0)  NOT NULL CONSTRAINT wyjazd_fk REFERENCES Wyjazd(Id_wyjazd),
	Id_klienta number(9,0) NOT NULL CONSTRAINT klient_fk REFERENCES Klient(Id_klienta),
    Ilosc_osob number(3,0) NOT NULL,
    Data_platnosc Date NOT NULL,
    Rodzaj_zamowienia varchar2(20) NOT NULL,
    transport varchar2(20) NOT NULL
);

CREATE TABLE Adres(
	Id_adresu number(15,0) NOT NULL CONSTRAINT adres_pk PRIMARY KEY,
	Miasto varchar2(20) NOT NULL ,
	Ulica varchar2(20) NOT NULL,
	Numer_lokalu number(2,0) NOT NULL,
	Numer_budynku number(3,0) NOT NULL 
);
    
Create TABLE Zakwaterowanie (
    Id_Zakwaterowanie number(15,0) NOT NULL CONSTRAINT zakwaterowanie_pk PRIMARY KEY,
    Id_wyjazdu number(15,0) NOT NULL CONSTRAINT wyjazd_zakwaterowanie_fk REFERENCES Wyjazd(Id_wyjazd),
	Id_adres number(15,0) NOT NULL  CONSTRAINT adres_zakwaterowanie_fk REFERENCES Adres(Id_adresu),
    Nazwa_zakwaterowania varchar2(20) NOT NULL,
    Jakosc_opcji number(1,0) NOT NULL,
    Cena_za_dobe number(5,2) NOT NULL,
    Liczba_pokoj number(3,0) NOT NULL
);

CREATE TABLE Transport(
    Id_transport number(15,0) NOT NULL CONSTRAINT transport_pk PRIMARY KEY,
    id_wyjazdu NOT NULL CONSTRAINT wyjazd_transport_fk REFERENCES Wyjazd(Id_wyjazd),
    Rodzaj_transportu varchar2(10) NOT NULL
);

CREATE TABLE Pociag(
	Id_pociag number(15,0) NOT NULL CONSTRAINT pociag_pk PRIMARY KEY,
	Id_transport number(15,0) NOT NULL CONSTRAINT pociag_transport_fk REFERENCES Transport(Id_transport), 
    Skad_wyjezdza varchar2(20) NOT NULL,
    Dokad_jedzie varchar2(20) NOT NULL,
    Cena_przejazdu number(3,0) NOT NULL

);
CREATE TABLE Samolot(
	Id_samolot number(15,0) NOT NULL CONSTRAINT samolot_pk PRIMARY KEY,
	Id_transport number(15,0) NOT NULL CONSTRAINT samolot_transport_fk REFERENCES Transport(Id_transport), 
    Skad_wylatuje varchar2(20) NOT NULL,
    Dokad_jedzie varchar2(20) NOT NULL,
    Cena_przelotu number(3,0) NOT NULL

);
CREATE TABLE Autobus(
	Id_autobusu number(15,0) NOT NULL CONSTRAINT autobus_pk PRIMARY KEY,
	Id_transport number(15,0) NOT NULL CONSTRAINT autobus_transport_fk REFERENCES Transport(Id_transport), 
    Skad_wyjezdza varchar2(20) NOT NULL,
    Dokad_jedzie varchar2(20) NOT NULL,
    Cena_przejazdu number(3,0) NOT NULL
);
commit;
