--Procedury i funkcje

--1)procedura wyswietlajaca zestawienie podanego roku   [uzytkownik podaje rok]
--domyslnie jest zwracana zestawienie wszystkich rokow [tylko typ number]
--(suma koszty )
--( --zrobic obsluge bledow w pythonie ) (dziala)
create or replace procedure zestawienie_rok 
(rok number DEFAULT 0 )is
    imie klient.imie%TYPE ;
    nazwisko klient.nazwisko%TYPE ;
    rodzaj_zamowienia zamowienie.rodzaj_zamowienia%TYPE ;
    cal_cena NUMBER;
    data_platnosc zamowienie.data_platnosc%TYPE;
    suma_kosztow  NUMBER := 0 ;
    wyc_transport zamowienie.transport%TYPE;
    wyc_ilosc_osob  zamowienie.ilosc_osob%TYPE;
    wyc_miejscowosc wyjazd.miejscowosc%TYPE;
    cena_transport Number(5);
    cena_transport_z_os Number(8);
    cursor domyslnie_wyswietl is 
        select kl.imie ,kl.nazwisko  ,  zam.rodzaj_zamowienia
        ,wyj.cena_za_osobe * zam.ilosc_osob + wyj.cena_wycieczki +(wyj.czas_trwania * zak.cena_za_dobe)  
        calkowita_cena,zam.data_platnosc platnosc 
        ,wyj.miejscowosc, zam.transport , zam.ilosc_osob 
        from zamowienie zam , wyjazd wyj , klient kl , zakwaterowanie zak
        where zam.id_wyjazdu = wyj.id_wyjazd  AND zam.id_klienta = kl.id_klienta AND  
        zak.id_wyjazdu = wyj.id_wyjazd; 
    cursor zestawienie_roczne is
        select kl.imie ,kl.nazwisko  ,  zam.rodzaj_zamowienia 
        ,wyj.cena_za_osobe * zam.ilosc_osob + wyj.cena_wycieczki +(wyj.czas_trwania * zak.cena_za_dobe)
        calkowita_cena,zam.data_platnosc platnosc  
        , wyj.miejscowosc, zam.transport , zam.ilosc_osob 
        from zamowienie zam , wyjazd wyj , klient kl , zakwaterowanie zak
        where zam.id_wyjazdu = wyj.id_wyjazd  AND zam.id_klienta = kl.id_klienta AND  
        zak.id_wyjazdu = wyj.id_wyjazd AND
        EXTRACT (YEAR FROM DATA_platnosc) = rok;
begin
if rok = 0  then
    dbms_output.put_line('zestawienie ogolne');
    open domyslnie_wyswietl;
    loop
        fetch domyslnie_wyswietl into  imie,nazwisko,rodzaj_zamowienia,cal_cena,data_platnosc,wyc_miejscowosc,wyc_transport,wyc_ilosc_osob;
        exit when domyslnie_wyswietl%NOTFOUND;
         if wyc_transport = 'autobus' then
                select cena_przejazdu into cena_transport From autobus where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cal_cena := cal_cena + cena_transport_z_os;
        
        elsif wyc_transport = 'pociag' then
                select cena_przejazdu into cena_transport From pociag where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cal_cena := cal_cena + cena_transport_z_os;
        else 
            select cena_przelotu into cena_transport From samolot where dokad_jedzie = wyc_miejscowosc ; 
            cena_transport_z_os := cena_transport * wyc_ilosc_osob;
            cal_cena := cal_cena + cena_transport_z_os;
        end if;     
        dbms_output.put_line('klient dane: ' || imie || ' ' || nazwisko || ' ' || 'typ zamowienia: '|| rodzaj_zamowienia 
        || ' ' || 'platnosc: '|| data_platnosc  ||' '|| ' calkowita cena wycieczki = ' ||' '|| cal_cena );
        suma_kosztow := suma_kosztow + cal_cena;
    end loop;
    close domyslnie_wyswietl;
    dbms_output.put_line('suma ze wszystkich lat ' ||' '|| suma_kosztow);

else
     dbms_output.put_line('zestawienie roku' ||' '|| rok );
     open zestawienie_roczne;
     loop
        fetch zestawienie_roczne into  imie,nazwisko,rodzaj_zamowienia,cal_cena,data_platnosc,wyc_miejscowosc,wyc_transport,wyc_ilosc_osob;
        exit when zestawienie_roczne%NOTFOUND;
         if wyc_transport = 'autobus' then
                select cena_przejazdu into cena_transport From autobus where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cal_cena := cal_cena + cena_transport_z_os;
        elsif wyc_transport = 'pociag' then
                select cena_przejazdu into cena_transport From pociag where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cal_cena := cal_cena + cena_transport_z_os;
        else 
            select cena_przelotu into cena_transport From samolot where dokad_jedzie = wyc_miejscowosc ; 
            cena_transport_z_os := cena_transport * wyc_ilosc_osob;
            cal_cena := cal_cena + cena_transport_z_os;
        end if; 
        dbms_output.put_line('klient dane: ' || imie || ' ' || nazwisko || ' ' || 'typ zamowienia: '|| rodzaj_zamowienia
        || ' ' ||'platnosc : '|| data_platnosc  ||' '|| ' calkowita cena wycieczki = ' ||' '|| cal_cena );
        suma_kosztow := suma_kosztow + cal_cena;
     end loop ;
     close zestawienie_roczne;
     dbms_output.put_line('suma dla zestawainia roku ' || ' ' || rok  ||' '||suma_kosztow);
end if;
end zestawienie_rok;
/
set serveroutput on
EXECUTE zestawienie_rok(2019);
EXECUTE zestawienie_rok();

--2 )funckja ktora zawiera dodawanie [klucz glowny jest przydzielanay za pomoca sekwencji] 
--dodawanie funckja (dziala)
create  sequence wprowadzaj_id 
    start with 11
    increment by 1
    maxvalue 1000000
    nocycle
    noorder;
create sequence wprowadzaj_id_klient
    start with 11
    increment by 1 
    maxvalue 1000000
    nocycle
    noorder;

create or replace function  wprowadzanie(
imie_klienta klient.imie%TYPE, nazwisko_klienta  klient.nazwisko%TYPE,
ile_osob zamowienie.ilosc_osob%TYPE,rodzaj_ulgi zamowienie.rodzaj_zamowienia%TYPE,
miasto wyjazd.miejscowosc%TYPE ,srodek_transportu transport.rodzaj_transportu%TYPE
)return varchar is 
    wyjazd_id wyjazd.id_wyjazd%TYPE;
    imie_sp klient.imie%TYPE;
    klient_id_prze klient.id_klienta%TYPE;
    nazwisko_sp klient.nazwisko%TYPE;
    miast_spr wyjazd.miejscowosc%TYPE;
    stan_sp number := 0;
    brak_pojazdu EXCEPTION;
    niewlasciwe_miasto EXCEPTION;
    zla_ulga EXCEPTION;
    cursor weryfikacja_klienta is
        Select id_klienta, imie , nazwisko From klient; 
    cursor sprawdzaj_miasto is 
        Select miejscowosc from wyjazd  where miejscowosc = miasto; 
begin

    open sprawdzaj_miasto;
        fetch sprawdzaj_miasto into miast_spr;
        if sprawdzaj_miasto%NOTFOUND  then 
            raise niewlasciwe_miasto;
        end if; 
    close sprawdzaj_miasto;
    if srodek_transportu != 'autobus' and srodek_transportu != 'samolot' and srodek_transportu != 'pociag' then
        raise brak_pojazdu;
    end if;
    if rodzaj_ulgi <> 'rodzinny' and rodzaj_ulgi <> 'dla par' and  rodzaj_ulgi <> 'normalny'  and  rodzaj_ulgi <> 'seniorski' and  rodzaj_ulgi <> 'firmowy' then
        raise zla_ulga;
    end if;
   select wyjazd.id_wyjazd into wyjazd_id FROM wyjazd where  wyjazd.miejscowosc = miasto;
   open weryfikacja_klienta;
    Loop
        exit when  weryfikacja_klienta%NOTFOUND;
        fetch  weryfikacja_klienta into  klient_id_prze ,imie_sp ,nazwisko_sp;
        if imie_klienta =  imie_sp AND nazwisko_klienta =  nazwisko_sp  then
            stan_sp := 1;
            insert into zamowienie values(wprowadzaj_id.nextval,wyjazd_id,klient_id_prze,ile_osob,SYSDATE,rodzaj_ulgi,srodek_transportu);
             return 'stary klient zamowil kolejna wycieczke';
        end if; 
    end loop;
   close weryfikacja_klienta;
if stan_sp = 0  then 
   insert into klient  values(wprowadzaj_id_klient.nextval,imie_klienta,nazwisko_klienta);
   insert into zamowienie values(wprowadzaj_id.nextval,wyjazd_id,wprowadzaj_id_klient.currval,ile_osob,SYSDATE,rodzaj_ulgi,srodek_transportu);
   commit;
   return 'dodano nowego klienta';
end if; 
EXCEPTION
WHEN brak_pojazdu then
    dbms_output.put_line('nie ma takiego srodka transportu');
    return null;
WHEN zla_ulga then 
    dbms_output.put_line('nie ma takiej ulgi');
    return null;
WHEN niewlasciwe_miasto then
    dbms_output.put_line('nie ma takiego miasta');
    return null;
end wprowadzanie;
/
set serveroutput on
--nowy klient 
VARIABLE nowa_osoba varchar2;
EXECUTE  :nowa_osoba  := wprowadzanie('Jarek','Jankowski',2,'normalny','Berlin','autobus');
PRINT nowa_osoba;
--stary klient
VARIABLE stary_klient varchar2;
EXECUTE  :stary_klient  :=  wprowadzanie('Pawel','Nowak',3,'firmowy','Moskwa','samolot');
PRINT stary_klient;

--3) funkcja usuwajaca klienta  i zamowienie [zabezpieczyc przed niewlasciwymi typami w poszczegolnych polach ]
create or replace function usuwanie(
imie_klienta klient.imie%TYPE,
nazwisko_klienta klient.nazwisko%TYPE,
wpr_id_zamowienia zamowienie.id_zamowienia%TYPE
)return varchar2 is
    cursor sprawdzaj_zamowienie is
        select id_zamowienia From zamowienie where id_klienta = (
        select id_klienta  from klient where imie = imie_klienta and nazwisko = nazwisko_klienta );
    cursor sprawdzaj_klienta is 
        select imie , nazwisko from klient;
    ilosc_zamowien number(2) :=  0 ;
    spr_id_zamowienia zamowienie.id_zamowienia%TYPE;
    stan_usun_zamowienia number(1) := 0;
    spr_imie    klient.imie%TYPE;   
    spr_nazwisko klient.nazwisko%TYPE;
    spr_stan number(1) :=0;
    brak_klienta EXCEPTION;
begin 
open sprawdzaj_klienta;
    Loop
        fetch sprawdzaj_klienta  into spr_imie, spr_nazwisko;
        exit when  sprawdzaj_klienta%NOTFOUND;
        if spr_imie =  imie_klienta and spr_nazwisko = nazwisko_klienta then
            spr_stan := 1;
        end if;
    end loop;
close sprawdzaj_klienta;
if  spr_stan = 0 then
    raise brak_klienta;
end if; 
open sprawdzaj_zamowienie;
    loop
        fetch sprawdzaj_zamowienie into  spr_id_zamowienia;
        exit when sprawdzaj_zamowienie%NOTFOUND;
        ilosc_zamowien := ilosc_zamowien + 1;
        if wpr_id_zamowienia  = spr_id_zamowienia then
                    stan_usun_zamowienia :=1;
        end if;
    end loop;
close sprawdzaj_zamowienie;
if stan_usun_zamowienia = 0 then
    return 'nie ma takiego zamowienia w bazie';
end if;

if  ilosc_zamowien = 1 then
    Delete FROM Zamowienie  where id_zamowienia =  wpr_id_zamowienia;
    Delete FROM Klient where imie =  imie_klienta and nazwisko = nazwisko_klienta ;
    commit;
    return 'usunieto klienta z bazy';
else
    Delete FROM Zamowienie  where id_zamowienia =  wpr_id_zamowienia;
    return 'usunieto zamowienie z bazy ';
end if;
EXCEPTION 
when  brak_klienta then
    dbms_output.put_line('nie ma takiego klienta');
    return null;
end usuwanie;
/
set serveroutput on 
--usuwanie tylko wycieczke
VARIABLE usun_osobe varchar2;
EXECUTE  :usun_osobe := usuwanie('asdsaddasy','asdasd',52223);
PRINT usun_osobe;
--usuwanie wycieczki wraz z klientem 
VARIABLE usun_osobe1 varchar2;
EXECUTE  :usun_osobe1 :=  usuwanie('Janusz','Antos',5);
PRINT usun_osobe1;

--4)funkcja aktualizaujaca  klienta [zabezpieczyc przed niewlasciwymi typami w poszczegolnych polach ]
create or replace function aktualizowanie (
imie_klienta klient.imie%TYPE,
nazwisko_klienta klient.nazwisko%TYPE,
wpr_id_zamowienia zamowienie.id_zamowienia%TYPE,
nowe_imie klient.imie%TYPE,
nowe_nazwisko klient.nazwisko%TYPE,
zmiana_ulgi zamowienie.rodzaj_zamowienia%TYPE,
nowy_transport zamowienie.transport%TYPE
)  return varchar2 is
 cursor sprawdzaj_zamowienie is
        select id_zamowienia From zamowienie where id_klienta = (
        select id_klienta  from klient where imie = imie_klienta and nazwisko = nazwisko_klienta );
 cursor sprawdzaj_klienta is 
        select imie , nazwisko from klient;
cursor sprawdzaj_osobe is 
        select imie , nazwisko from klient where imie = nowe_imie and  nazwisko = nowe_nazwisko; 
    spr_imie    klient.imie%TYPE;   
    spr_nazwisko klient.nazwisko%TYPE;
    spr_stan number(1) :=0;
    spr_id_zamowienia zamowienie.id_zamowienia%TYPE;
    stan_aktualizowania_zamowienia number(1) := 0;
    brak_klienta EXCEPTION;
    brak_zamowienia EXCEPTION;
    zla_ulga EXCEPTION;
    zly_transport  EXCEPTION;
    niewlasciwe_osoba EXCEPTION;
begin 
open sprawdzaj_klienta;
    Loop
        fetch sprawdzaj_klienta  into spr_imie, spr_nazwisko;
        exit when  sprawdzaj_klienta%NOTFOUND;
        if spr_imie =  imie_klienta and spr_nazwisko = nazwisko_klienta then
            spr_stan := 1;
        end if;
    end loop;
close sprawdzaj_klienta;
if  spr_stan = 0 then
    raise brak_klienta;
end if; 
open sprawdzaj_zamowienie;
    loop
        fetch sprawdzaj_zamowienie into  spr_id_zamowienia;
        exit when sprawdzaj_zamowienie%NOTFOUND;
        if wpr_id_zamowienia  = spr_id_zamowienia then
                    stan_aktualizowania_zamowienia :=1;
        end if;
    end loop;
close sprawdzaj_zamowienie;
if stan_aktualizowania_zamowienia = 0 then 
    raise brak_zamowienia;
end if;

open sprawdzaj_osobe;
    fetch sprawdzaj_osobe into spr_imie, spr_nazwisko;
    if sprawdzaj_osobe%FOUND  then 
        raise niewlasciwe_osoba;
    end if; 
close sprawdzaj_osobe;
Update klient set nazwisko = nowe_nazwisko where imie = imie_klienta and  nazwisko = nazwisko_klienta;
commit;
Update klient set imie = nowe_imie  where imie = imie_klienta and  nazwisko = nazwisko_klienta;
commit;
if zmiana_ulgi <> 'rodzinny' and zmiana_ulgi <> 'dla par' and  zmiana_ulgi <> 'normalny'  
and  zmiana_ulgi <> 'seniorski' and  zmiana_ulgi <> 'firmowy' then
    raise zla_ulga;
end if;
update zamowienie set rodzaj_zamowienia = zmiana_ulgi where id_zamowienia = wpr_id_zamowienia;

if  nowy_transport <> 'pociag' and nowy_transport <> 'samolot' and  nowy_transport <> 'autobus'  then
    raise zly_transport;
end if;
update zamowienie set transport = nowy_transport where id_zamowienia = wpr_id_zamowienia;

EXCEPTION 
when  brak_klienta then
    dbms_output.put_line('nie ma takiego klienta');
    return 'nie ma takiego klienta';
when brak_zamowienia then
    dbms_output.put_line('nie ma zamowienia w bazie');
    return 'nie ma zamowienia w bazie';
when niewlasciwe_osoba then
    dbms_output.put_line(' nie istnieje takie osoba w bazie');
    return ' nie istnieje takie osoba w bazie';
when zla_ulga then
    dbms_output.put_line('nie ma takiej ulgi');
    return null;
when zly_transport then
    dbms_output.put_line('nie ma takiego dostepnego transportu');
    return null;
end aktualizowanie;
/
set serveroutput on 
VARIABLE aktualizuj_osobe varchar2;
EXECUTE  :aktualizuj_osobe := aktualizowanie('Pawel','Nowak',2,'firmowy','firmowy','firmowy','firmowy');
PRINT aktualizuj_osobe;
EXECUTE  :aktualizuj_osobe := aktualizowanie('Pawel','Nowak',2,'Monika','Nowak','firmowy','firmowy');
PRINT aktualizuj_osobe;
EXECUTE  :aktualizuj_osobe := aktualizowanie('Monika','Nowak',2,'Monika','Kowalska','rodzinny','pociag');
PRINT aktualizuj_osobe;

--(zweryfikowac tabele samolot, pociag ,autobus , transport)
--5)procedura wyswietlaja wszystkie trasy okreslonego transportu
create or replace procedure  rodzaj_tran(srodek_transportu transport.rodzaj_transportu%TYPE) is
    cursor wyswietl_tr_aut is
        select aut.skad_wyjezdza ,aut.dokad_jedzie,aut.cena_przejazdu
        from transport tr ,autobus aut
        where tr.id_transport = aut.id_transport AND tr.rodzaj_transportu = 'autobus';
    cursor wyswietl_tr_sam is
        select  sam.skad_wylatuje ,sam.dokad_jedzie,sam.cena_przelotu
        from transport tr ,samolot sam 
        where tr.id_transport = sam.id_transport AND tr.rodzaj_transportu = 'samolot';
    cursor wyswietl_tr_poc is
        select  poc.skad_wyjezdza,poc.dokad_jedzie,poc.cena_przejazdu
        from transport tr , pociag poc 
        where tr.id_transport = poc.id_transport AND tr.rodzaj_transportu = 'pociag';
    
    skad pociag.skad_wyjezdza%TYPE;
    dokad pociag.dokad_jedzie%TYPE;
    cena pociag.cena_przejazdu%TYPE;
begin 
    if srodek_transportu = 'autobus' then 
        open wyswietl_tr_aut;
            LOOP
                fetch wyswietl_tr_aut into skad,dokad,cena;
                exit when wyswietl_tr_aut%NOTFOUND;
                dbms_output.put_line(srodek_transportu  ||' '|| skad ||' '|| dokad ||' ' || cena);
            end LOOP;
        close wyswietl_tr_aut;  
    elsif srodek_transportu = 'samolot' then 
        open  wyswietl_tr_sam;
            LOOP
                fetch wyswietl_tr_sam into skad,dokad,cena;
                exit when wyswietl_tr_sam%NOTFOUND;
                dbms_output.put_line(srodek_transportu ||' '|| skad ||' '|| dokad ||' ' || cena);
            end LOOP;
        close wyswietl_tr_sam;
    elsif srodek_transportu = 'pociag' then
        open wyswietl_tr_poc;
            LOOP
                fetch wyswietl_tr_poc into skad,dokad,cena;
                exit when wyswietl_tr_poc%NOTFOUND;
                dbms_output.put_line(srodek_transportu ||' '|| skad ||' '|| dokad ||' ' || cena);
            end LOOP;
        close wyswietl_tr_poc;
    end if;
end rodzaj_tran;
/
set serveroutput on
execute rodzaj_tran('samolot');

--zrobic checkbox w 
--6)procedure  ktora  wyswietla suma wycieczki uzgledniajac ulge --(zrobione)
create or replace procedure suma_z_ulga(zamowienie_rodzaj in zamowienie.rodzaj_zamowienia%TYPE) as
    cursor normalny is
        select kl.imie ,kl.nazwisko  ,
        wyj.cena_za_osobe * zam.ilosc_osob + wyj.cena_wycieczki +(wyj.czas_trwania * zak.cena_za_dobe) as calkowita_cena
        ,wyj.miejscowosc, zam.transport , zam.ilosc_osob 
        from zamowienie zam , wyjazd wyj , klient kl , zakwaterowanie zak
        where zam.id_wyjazdu = wyj.id_wyjazd  
        AND zam.id_klienta = kl.id_klienta
        AND  zak.id_wyjazdu = wyj.id_wyjazd
        AND zam.rodzaj_zamowienia = 'normalny' ;
        
    cursor dla_par is
        select kl.imie ,kl.nazwisko  ,
        wyj.cena_za_osobe * zam.ilosc_osob + wyj.cena_wycieczki +(wyj.czas_trwania * zak.cena_za_dobe) as calkowita_cena
        ,wyj.miejscowosc, zam.transport , zam.ilosc_osob 
        from zamowienie zam , wyjazd wyj , klient kl , zakwaterowanie zak
        where zam.id_wyjazdu = wyj.id_wyjazd  
        AND zam.id_klienta = kl.id_klienta
        AND  zak.id_wyjazdu = wyj.id_wyjazd
        AND zam.rodzaj_zamowienia = 'dla par' ;
        
    cursor seniorski is
        select kl.imie ,kl.nazwisko  ,
        wyj.cena_za_osobe * zam.ilosc_osob + wyj.cena_wycieczki +(wyj.czas_trwania * zak.cena_za_dobe) as calkowita_cena_po_obnizce
        ,wyj.miejscowosc, zam.transport , zam.ilosc_osob 
        from zamowienie zam , wyjazd wyj , klient kl , zakwaterowanie zak
        where zam.id_wyjazdu = wyj.id_wyjazd  
        AND zam.id_klienta = kl.id_klienta
        AND  zak.id_wyjazdu = wyj.id_wyjazd
        AND zam.rodzaj_zamowienia = 'seniorski' ;
    
    cursor firmowy is
        select kl.imie ,kl.nazwisko  ,
        wyj.cena_za_osobe * zam.ilosc_osob + wyj.cena_wycieczki +(wyj.czas_trwania * zak.cena_za_dobe) as calkowita_cena
        ,wyj.miejscowosc, zam.transport , zam.ilosc_osob 
        from zamowienie zam , wyjazd wyj , klient kl , zakwaterowanie zak
        where zam.id_wyjazdu = wyj.id_wyjazd  
        AND zam.id_klienta = kl.id_klienta
        AND  zak.id_wyjazdu = wyj.id_wyjazd
        AND zam.rodzaj_zamowienia = 'firmowy' ;
    
    cursor rodzinny is 
        select kl.imie ,kl.nazwisko  ,
        wyj.cena_za_osobe * zam.ilosc_osob + wyj.cena_wycieczki +(wyj.czas_trwania * zak.cena_za_dobe) as calkowita_cena
        ,wyj.miejscowosc, zam.transport , zam.ilosc_osob 
        from zamowienie zam , wyjazd wyj , klient kl , zakwaterowanie zak
        where zam.id_wyjazdu = wyj.id_wyjazd  
        AND zam.id_klienta = kl.id_klienta
        AND  zak.id_wyjazdu = wyj.id_wyjazd
        AND zam.rodzaj_zamowienia = 'rodzinny' ;
    
    suma_ulga number(6) :=0 ;
    imie_kl  klient.imie%TYPE;
    nazwisko_kl klient.nazwisko%TYPE;
    cena_kl  number(6);
    wyc_transport zamowienie.transport%TYPE;
    wyc_ilosc_osob  zamowienie.ilosc_osob%TYPE;
    wyc_miejscowosc wyjazd.miejscowosc%TYPE;
    cena_transport Number(5);
    cena_transport_z_os Number(8);
begin 
if  zamowienie_rodzaj = 'normalny' then
    open normalny;
        loop
            fetch normalny INTO imie_kl,nazwisko_kl,cena_kl,wyc_miejscowosc,wyc_transport,wyc_ilosc_osob;
             exit when normalny%NOTFOUND;
             if wyc_transport = 'autobus' then
                select cena_przejazdu into cena_transport From autobus where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := cena_kl + cena_transport_z_os;
             elsif wyc_transport = 'pociag' then
                select cena_przejazdu into cena_transport From pociag where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := cena_kl + cena_transport_z_os;
           else 
                select cena_przelotu into cena_transport From samolot where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := cena_kl + cena_transport_z_os;
        end if; 
        dbms_output.put_line(imie_kl ||' '|| nazwisko_kl ||' '|| cena_kl);
        suma_ulga := suma_ulga + cena_kl;    
        end loop;
    close normalny;
    dbms_output.put_line('suma = ' || suma_ulga );
    
elsif zamowienie_rodzaj = 'dla par' then
     open dla_par;
        loop
            fetch dla_par INTO  imie_kl,nazwisko_kl,cena_kl,wyc_miejscowosc,wyc_transport,wyc_ilosc_osob;
            exit when dla_par%NOTFOUND;
             if wyc_transport = 'autobus' then
                select cena_przejazdu into cena_transport From autobus where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := ((cena_kl + cena_transport_z_os )/10)*9.5;
             elsif wyc_transport = 'pociag' then
                select cena_przejazdu into cena_transport From pociag where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := ((cena_kl + cena_transport_z_os )/10)*9.5;
           else 
                select cena_przelotu into cena_transport From samolot where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := ((cena_kl + cena_transport_z_os )/10)*9.5;
            end if;
            dbms_output.put_line(imie_kl ||' '|| nazwisko_kl ||' '|| cena_kl);
            suma_ulga := suma_ulga + cena_kl;
        end loop;
    close dla_par;
    dbms_output.put_line('suma = ' || suma_ulga );
    
elsif zamowienie_rodzaj = 'seniorski' then 
    open seniorski;
        loop
            fetch seniorski INTO  imie_kl,nazwisko_kl,cena_kl,wyc_miejscowosc,wyc_transport,wyc_ilosc_osob;
            exit when seniorski%NOTFOUND;
             if wyc_transport = 'autobus' then
                select cena_przejazdu into cena_transport From autobus where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := ((cena_kl + cena_transport_z_os )/10)*8;
             elsif wyc_transport = 'pociag' then
                select cena_przejazdu into cena_transport From pociag where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := ((cena_kl + cena_transport_z_os )/10)*8;
           else 
                select cena_przelotu into cena_transport From samolot where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := ((cena_kl + cena_transport_z_os )/10)*8;
            end if;
            dbms_output.put_line(imie_kl ||' '|| nazwisko_kl ||' '|| cena_kl);
            suma_ulga := suma_ulga + cena_kl;
        end loop;
    close seniorski;
    dbms_output.put_line('suma = ' || suma_ulga );

elsif zamowienie_rodzaj = 'firmowy' then 
    open firmowy;
        loop
            fetch firmowy INTO  imie_kl,nazwisko_kl,cena_kl,wyc_miejscowosc,wyc_transport,wyc_ilosc_osob;
            exit when firmowy%NOTFOUND;
            if wyc_transport = 'autobus' then
                select cena_przejazdu into cena_transport From autobus where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := ((cena_kl + cena_transport_z_os )/10)*9;
             elsif wyc_transport = 'pociag' then
                select cena_przejazdu into cena_transport From pociag where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := ((cena_kl + cena_transport_z_os )/10)*9;
           else 
                select cena_przelotu into cena_transport From samolot where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := ((cena_kl + cena_transport_z_os )/10)*9;
            end if;
            dbms_output.put_line(imie_kl ||' '|| nazwisko_kl ||' '|| cena_kl);
            suma_ulga := suma_ulga + cena_kl;
        end loop;
    close firmowy;
    dbms_output.put_line('suma = ' || suma_ulga );
    
elsif zamowienie_rodzaj = 'rodzinny' then 
open rodzinny;
        loop
            fetch rodzinny INTO  imie_kl,nazwisko_kl,cena_kl,wyc_miejscowosc,wyc_transport,wyc_ilosc_osob;
            exit when rodzinny%NOTFOUND;
            if wyc_transport = 'autobus' then
                select cena_przejazdu into cena_transport From autobus where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := ((cena_kl + cena_transport_z_os )/10)*9.1;
             elsif wyc_transport = 'pociag' then
                select cena_przejazdu into cena_transport From pociag where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := ((cena_kl + cena_transport_z_os )/10)*9.1;
           else 
                select cena_przelotu into cena_transport From samolot where dokad_jedzie = wyc_miejscowosc ; 
                cena_transport_z_os := cena_transport * wyc_ilosc_osob;
                cena_kl := ((cena_kl + cena_transport_z_os )/10)*9.1;
            end if;
            dbms_output.put_line(imie_kl ||' '|| nazwisko_kl ||' '|| cena_kl);
            suma_ulga := suma_ulga + cena_kl;
        end loop;
    close rodzinny;
    dbms_output.put_line('suma = ' || suma_ulga );
end if;
end suma_z_ulga;
/
set serveroutput on
execute suma_z_ulga('seniorski');
execute suma_z_ulga('normalny');
execute suma_z_ulga('firmowy');
execute suma_z_ulga('dla par');
execute suma_z_ulga('rodzinny');

--7)procedura wypisujaca  wszystko o podanym uzytkowniku 
--[ zabezpieczyc funckje przed innymi typami niz varchar2--(dziala)
create or replace procedure dane_klient_wys (imie_kl in klient.imie%TYPE ,nazwiko_kl in klient.nazwisko%TYPE ) is
cursor statystyka_klienta is 
    select  zam.ilosc_osob il_osob,zam.rodzaj_zamowienia ,zam.data_platnosc, 
    tr.rodzaj_transportu,wyj.kraj,wyj.miejscowosc miasto,        
    wyj.cena_za_osobe * zam.ilosc_osob + wyj.cena_wycieczki +(wyj.czas_trwania * zak.cena_za_dobe) as cal_cena
    
    from klient kl,zakwaterowanie zak , wyjazd wyj,transport tr  ,zamowienie zam
    where zam.id_klienta = kl.id_klienta AND zam.id_wyjazdu = wyj.id_wyjazd AND zak.id_wyjazdu = wyj.id_wyjazd
    AND wyj.id_wyjazd = tr.id_wyjazdu AND kl.imie = imie_kl AND kl.nazwisko = nazwiko_kl; 
    
ile_osob  zamowienie.ilosc_osob%TYPE;
ulga  zamowienie.rodzaj_zamowienia%TYPE;
platnosc  zamowienie.data_platnosc%TYPE;
rodzaj_transportu transport.rodzaj_transportu%TYPE;
kraj_wyj  wyjazd.kraj%TYPE;
miasto  wyjazd.miejscowosc%TYPE;
cal_calkowita number(8);
cena_transport Number(5);
cena_transport_z_os Number(8);
suma_z_ulga NUMBER(8);
begin
    open statystyka_klienta;
    loop
        fetch statystyka_klienta into ile_osob,ulga,platnosc,rodzaj_transportu,kraj_wyj,miasto,cal_calkowita;
        exit when statystyka_klienta%NOTFOUND;
         if rodzaj_transportu = 'autobus' then
                select cena_przejazdu into cena_transport From autobus where dokad_jedzie = miasto ; 
                cena_transport_z_os := cena_transport * ile_osob;
               cal_calkowita := cal_calkowita + cena_transport_z_os; 
             elsif rodzaj_transportu = 'pociag' then
                select cena_przejazdu into cena_transport From pociag where dokad_jedzie = miasto ; 
                cena_transport_z_os := cena_transport *ile_osob;
                cal_calkowita := cal_calkowita + cena_transport_z_os;
           else 
                select cena_przelotu into cena_transport From samolot where dokad_jedzie = miasto ; 
                cena_transport_z_os := cena_transport * ile_osob;
                cal_calkowita := cal_calkowita + cena_transport_z_os ;
            end if;
        if ulga  = 'rodzinny' then 
         suma_z_ulga  := (cal_calkowita/10)*9.1;
        elsif ulga = 'seniorski' then
            suma_z_ulga  := (cal_calkowita/10)*8;
        elsif ulga = 'firmowy' then
            suma_z_ulga  := (cal_calkowita/10)*9;
        elsif ulga = 'dla par' then
             suma_z_ulga  := (cal_calkowita/10)*9.5;
        else 
             suma_z_ulga  := cal_calkowita;
        end if;
        dbms_output.put_line( imie_kl ||' '|| nazwiko_kl ||' '|| ile_osob ||' '|| ulga ||' '|| platnosc ||' '|| rodzaj_transportu ||' '||
        kraj_wyj ||' '|| miasto ||' '|| cal_calkowita || ' po uwzglednienui ulgi: ' || suma_z_ulga );
    end loop;
    close statystyka_klienta;
end dane_klient_wys;
/
set serveroutput on
execute dane_klient_wys('Bartek','Boruc');
execute dane_klient_wys('Pawel','Nowak');

--8)procedura wyswietlajaca klientowi  wszystkie wycieczki {zab przed innymi wartosciami niz string } 
--przez podanie kraju i miasta--( --zrobic obsluge bledow w python) --(dziala)
create or replace procedure  wyswietl_klientowi 
(kraj1 in wyjazd.kraj%TYPE, miasto  wyjazd.miejscowosc%TYPE) as
    czas_trwania wyjazd.czas_trwania%TYPE;
    cena_wycieczki wyjazd.cena_wycieczki%TYPE;
    cena_za_osobe  wyjazd.cena_za_osobe%TYPE; 
    kraj_pr  wyjazd.kraj%TYPE;
    miasto_pr wyjazd.kraj%TYPE;
    cursor wyswietl_stan_miasto__kraj is
         select wyj.kraj , wyj.miejscowosc , wyj.czas_trwania ,wyj.cena_wycieczki,wyj.cena_za_osobe
         from wyjazd wyj where wyj.kraj = kraj1 and wyj.miejscowosc = miasto;
    cursor wyswietl_stan_kraj is
         select wyj.kraj , wyj.miejscowosc , wyj.czas_trwania ,wyj.cena_wycieczki,wyj.cena_za_osobe 
         from wyjazd wyj where wyj.kraj = kraj1;
begin
if miasto is NULL then  
    open wyswietl_stan_kraj;
     loop
        fetch wyswietl_stan_kraj INTO   kraj_pr , miasto_pr  ,czas_trwania ,cena_wycieczki ,cena_za_osobe;
        exit when  wyswietl_stan_kraj%NOTFOUND;
        dbms_output.put_line('kraj podrozy: ' || kraj_pr||' '|| 'miasto: '  || miasto_pr || ' '
        || 'czas trwania wycieczki' ||' '|| czas_trwania ||'dni'   ||' '|| 'cena wycieczki: ' ||
        cena_wycieczki  ||' '|| 'cena za osobe: '|| cena_za_osobe);
     end loop;
    close wyswietl_stan_kraj;
else 
   open wyswietl_stan_miasto__kraj;
     loop
        fetch wyswietl_stan_miasto__kraj into  kraj_pr , miasto_pr  ,czas_trwania ,cena_wycieczki ,cena_za_osobe;
        exit when wyswietl_stan_miasto__kraj%NOTFOUND;
        dbms_output.put_line('kraj podrozy: ' || kraj_pr||' '|| 'miasto: '  || miasto_pr || ' '
        || 'czas trwania wycieczki' ||' '|| czas_trwania ||'dni'   ||' '|| 'cena wycieczki: ' ||
        cena_wycieczki  ||' '|| 'cena za osobe: '|| cena_za_osobe);
     end loop ;
    close  wyswietl_stan_miasto__kraj;
end if;
end wyswietl_klientowi;
/
set serveroutput on
EXECUTE wyswietl_klientowi('Rosja','Moskwa');
EXECUTE wyswietl_klientowi('Rosja','');

EXEC biuro_podrozy.zestawienie_rok();


