



--Wyzwalacze

--1)po wprowadzeniu klienta (chodzi o zAMOWIENIE) odejmu sie liczbe pokoi  z tabeli zakwaterowanie (po insert) 
create or replace trigger odej_pokoje 
After insert on zamowienie
for each row 
liczba_pok zakwaterowanie.liczba_pokoj%TYPE;
wartosc zakwaterowanie.liczba_pokoj%TYPE;
begin 
select liczba_pokoj into liczba_pok from  zamowienia where id_wyjazdu = :old.id_wyjazdu;

    wartosc := liczba_pok - :old.ilosc_osob;
    


end odej_pokoje;
/

--2)po usunieciu klienta przywraca sie wartosc liczby pokoi z tabeli zakwaterowanie   
--przed wprowadzeniem  tego klienta (po delete)
create or replace trigger dodaj_pokoj
after delete on klient
 for each row 
 begin
  end dodaj_pokoj;
 /
--3)po wprowadzenie  zamowieni tabela wyjazd WYSwietla suma cene za osobe [np  3 osoby 
--* cena_za osobe] {w jednej wycieczce} ]
create or replace trigger cena_za_osobe
after insert on zamowienie
 for each row 
 begin
 end cena_za_osobe;
 /
--4) po wprowadzenie zamoweienia wyswietla suma wszystkich zamowien i po usunieciu zamowienia 
create or replace trigger wyswietl_sume_zamowien
after insert on zamowienie
for each row 
 begin
end wyswietl_sume_zamowien;
/

 --5)jesli   klient zostanie zaktualizowany wyswietla liste nie posortowano klientow
create or replace trigger zak_pos
 after update on klient
 for each row 
 begin
 open wyswietl;
    loop
        dbms_output.put_line('zaktualizowano rekord w klient ');
    end loop;
close wyswietl;
 end zak_pos;
 /
 set serveroutput on
 alter trigger  zak_pos ENABLE;
 update klient set imie = 'Patryk'  where id_klienta=4;
 
 
 
